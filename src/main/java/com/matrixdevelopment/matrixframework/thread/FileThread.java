package com.matrixdevelopment.matrixframework.thread;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.usermanagement.UserData;
import com.matrixdevelopment.matrixframework.util.files.FilesManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class FileThread {

    static FileThread INSTANCE = new FileThread();

    /**
     * Gets the single instance of Thread.
     *
     * @return single instance of Thread
     */
    public static FileThread getInstance() {
        return INSTANCE;
    }

    MatrixFramework plugin = MatrixFramework.getInstance();

    private ReadThread thread;

    public class ReadThread extends java.lang.Thread {

        public void deletePlayerFile(String uuid) {
            synchronized (FileThread.getInstance()) {
                try {
                    File dFile = new File(MatrixFramework.getInstance().getPlugin().getDataFolder() + File.separator + "data", uuid + ".yml");

                    if (dFile.exists()) {
                        dFile.delete();
                    }
                } catch (Exception e) {
                    MatrixFramework.getInstance().debug(e);
                }
            }
        }

        public FileConfiguration getData(UserData userData, String uuid) {
            synchronized (FileThread.getInstance()) {
                try {
                    File dFile = getPlayerFile(uuid);

                    if (dFile != null) {
                        FileConfiguration data = YamlConfiguration.loadConfiguration(dFile);
                        return data;
                    }
                } catch (Exception e) {
                    MatrixFramework.getInstance().debug(e);
                }

                MatrixFramework.getInstance().getPlugin().getLogger().warning("Failed to load " + uuid + ".yml, enable debug to see full stacktraces.");
                return null;
            }
        }

        public File getPlayerFile(String uuid) {
            synchronized (FileThread.getInstance()) {
                try {
                    File dFile = new File(MatrixFramework.getInstance().getPlugin().getDataFolder() + File.separator + "data", uuid + ".yml");
                    FileConfiguration data = YamlConfiguration.loadConfiguration(dFile);

                    if (!dFile.exists()) {
                        FilesManager.getInstance().editFile(dFile, data);
                    }

                    return dFile;
                } catch (Exception e) {
                    MatrixFramework.getInstance().debug(e);
                }

                MatrixFramework.getInstance().getPlugin().getLogger().warning("Failed to load " + uuid + ".yml, enable debug to see full stacktraces");
                return null;
            }
        }

        public boolean hasPlayerFile(String uuid) {
            synchronized (FileThread.getInstance()) {

                try {
                    File dFile = new File(MatrixFramework.getInstance().getPlugin().getDataFolder() + File.separator + "data", uuid + ".yml");
                    return dFile.exists();
                } catch (Exception e) {
                    MatrixFramework.getInstance().debug(e);
                }

                return false;
            }
        }

        @Override
        public void run() {
            while (true) {
                try {
                    sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.exit(0);
                }
            }
        }


        /**
         * Run...
         *
         * @param run Run
         */
        public void run(Runnable run) {
            synchronized (FileThread.getInstance()) {
                run.run();
            }
        }

        public void setData(UserData userData, final String uuid, final String path, final Object value) {
            synchronized (FileThread.getInstance()) {
                try {
                    File dFile = getPlayerFile(uuid);
                    FileConfiguration data = getData(userData, uuid);
                    data.set(path, value);
                    data.save(dFile);
                } catch (Exception e) {
                    MatrixFramework.getInstance().getPlugin().getLogger().warning("Failed to set a value for " + uuid + ".yml, enabled debug to see full stacktraces");
                    MatrixFramework.getInstance().debug(e);
                }
            }
        }
    }

    /**
     * Instantiates a new thread
     */
    private FileThread() {}

    /**
     * @return the Thread
     */
    public ReadThread getThread() {
        if (thread == null || !thread.isAlive()) {
            plugin.debug("Loading thread");
            loadThread();
        }
        return thread;
    }

    /**
     * Load Thread
     */
    public void loadThread() {
        thread = new ReadThread();
        thread.start();
    }

    /**
     * Run...
     *
     * @param run Run
     */
    public void run(Runnable run) {
        getThread().run(run);
    }

}
