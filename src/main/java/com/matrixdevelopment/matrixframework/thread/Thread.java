package com.matrixdevelopment.matrixframework.thread;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.util.misc.NameFetcher;

import java.util.Arrays;

public class Thread {

    public class ReadThread extends java.lang.Thread {

        public String getName(java.util.UUID uuid) {
            plugin.debug("Looking up player name: " + uuid);
            NameFetcher fet = new NameFetcher(Arrays.asList(uuid));
            try {
                return fet.call().get(uuid);
            } catch (Exception e) {
                return "";
            }
        }

        @Override
        public void run() {
            while (true) {
                try {
                    sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.exit(0);
                }
            }
        }

        /**
         * Run.
         *
         * @param run
         *            the run
         */
        public void run(Runnable run) {
            synchronized (Thread.getInstance()) {
                run.run();
            }
        }
    }

    static Thread instance = new Thread();

    public static Thread getInstance() {
        return instance;
    }

    MatrixFramework plugin = MatrixFramework.getInstance();

    private ReadThread thread;

    private Thread() {}

    public ReadThread getThread() {
        if (thread == null || !thread.isAlive()) {
            loadThread();
        }
        return thread;
    }

    public void loadThread() {
        thread = new ReadThread();
        thread.start();
    }

    public void run(Runnable run) {
        getThread().run(run);
    }

}
