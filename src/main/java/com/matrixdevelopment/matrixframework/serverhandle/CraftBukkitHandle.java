package com.matrixdevelopment.matrixframework.serverhandle;

import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.entity.Player;

public class CraftBukkitHandle implements IServerHandle {

    @Override
    public void sendMessage(Player player, BaseComponent component) {
        player.sendMessage(component.toLegacyText());
    }

    @Override
    public void sendMessage(Player player, BaseComponent... components) {
        for (BaseComponent component : components) {
            sendMessage(player, component);
        }
    }

}
