package com.matrixdevelopment.matrixframework.serverhandle;

import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.entity.Player;

public interface IServerHandle {

    void sendMessage(Player player, BaseComponent component);

    void sendMessage(Player player, BaseComponent... components);

}
