package com.matrixdevelopment.matrixframework.serverhandle;

import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.entity.Player;

public class SpigotHandle implements IServerHandle {

    @Override
    public void sendMessage(Player player, BaseComponent component) {
        player.spigot().sendMessage(component);
    }

    @Override
    public void sendMessage(Player player, BaseComponent... components) {
        player.spigot().sendMessage(components);
    }

}
