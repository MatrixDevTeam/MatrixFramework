package com.matrixdevelopment.matrixframework.time.events;

import com.matrixdevelopment.matrixframework.time.TimeType;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class DateChangeEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public static HandlerList getHandlerList() {
        return handlers;
    }

    private boolean fake;
    private TimeType timeType;

    public DateChangeEvent(TimeType time) {
        super(true);
        this.timeType = time;
    }

    /**
     * (non-Javadoc)
     *
     * @see Event#getHandlers()
     */
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public TimeType getTimeType() {
        return timeType;
    }

    public boolean isFake() {
        return fake;
    }

    public void setFake(boolean fake) {
        this.fake = false;
    }

    public void setTimeType(TimeType timeType) {
        this.timeType = timeType;
    }

}
