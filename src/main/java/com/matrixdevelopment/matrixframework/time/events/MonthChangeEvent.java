package com.matrixdevelopment.matrixframework.time.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class MonthChangeEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public static HandlerList getHandlerList() {
        return handlers;
    }

    private boolean fake;

    public MonthChangeEvent() {
        super(true);
    }

    /**
     * (non-Javadoc)
     *
     * @see Event#getHandlers()
     */
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public boolean isFake() {
        return fake;
    }

    public void setFake(boolean fake) {
        this.fake = fake;
    }
}
