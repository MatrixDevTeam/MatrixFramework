package com.matrixdevelopment.matrixframework.time;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.data.ServerData;
import com.matrixdevelopment.matrixframework.time.events.*;

import java.time.LocalDateTime;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class TimeChecker {

    static TimeChecker INSTANCE = new TimeChecker();

    public static TimeChecker getInstance() {
        return INSTANCE;
    }

    MatrixFramework plugin = MatrixFramework.getInstance();

    private TimeChecker() {}

    public void forceChanged(TimeType time) {
        forceChanged(time, true);
    }

    public void forceChanged(TimeType time, boolean fake) {
        plugin.debug("Executing time change events: " + time.toString());
        plugin.getPlugin().getLogger().info("Time change: " + time.toString() + ", Fake: " + fake);
        PreDateChangeEvent preDateChange = new PreDateChangeEvent(time);
        preDateChange.setFake(fake);
        plugin.getPlugin().getServer().getPluginManager().callEvent(preDateChange);

        if (time.equals(TimeType.DAY)) {
            DayChangeEvent dayChange = new DayChangeEvent();
            dayChange.setFake(fake);
            plugin.getPlugin().getServer().getPluginManager().callEvent(dayChange);
        } else if (time.equals(TimeType.WEEK)) {
            WeekChangeEvent weekChange = new WeekChangeEvent();
            weekChange.setFake(fake);
            plugin.getPlugin().getServer().getPluginManager().callEvent(weekChange);
        } else if (time.equals(TimeType.MONTH)) {
            MonthChangeEvent monthChange = new MonthChangeEvent();
            monthChange.setFake(fake);
            plugin.getPlugin().getServer().getPluginManager().callEvent(monthChange);
        }

        DateChangeEvent dateChange = new DateChangeEvent(time);
        dateChange.setFake(fake);
        plugin.getPlugin().getServer().getPluginManager().callEvent(dateChange);

        plugin.debug("Finished executing time change events: " + time.toString());
    }

    public LocalDateTime getTime() {
        return LocalDateTime.now().plusHours(MatrixFramework.getInstance().getTimeHourOffSet());
    }

    /**
     * Checks for day changed.
     *
     * @return true, if successful
     */
    public boolean hasDayChanged() {
        int prevDay = ServerData.getInstance().getPreviousDay();
        int day = getTime().getDayOfMonth();

        if (prevDay == day) {
            return false;
        }
        ServerData.getInstance().setPreviousDay(day);
        if (prevDay == -1) {
            return false;
        }
        return true;
    }

    /**
     * Checks for month changed.
     *
     * @return true, if successful
     */
    public boolean hasMonthChanged() {
        String prevMonth = ServerData.getInstance().getPreviousMonth();
        String month = getTime().getMonth().toString();
        if (prevMonth.equals(month)) {
            return false;
        }
        ServerData.getInstance().setPreviousMonth(month);
        return true;
    }

    public boolean hasTimeOffSet() {
        return MatrixFramework.getInstance().getTimeHourOffSet() != 0;
    }

    /**
     * Checks for week changed.
     *
     * @return true, if successful
     */
    public boolean hasWeekChanged() {
        int prevDate = ServerData.getInstance().getPreviousWeekDay();
        LocalDateTime date = getTime();
        TemporalField woy = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();
        int weekNumber = date.get(woy);
        if (weekNumber == prevDate) {
            return false;
        }
        ServerData.getInstance().setPreviousWeekDay(weekNumber);
        if (prevDate == -1) {
            return false;
        }
        return true;
    }

    /**
     * Update.
     */
    public void update() {
        if (hasTimeOffSet()) {
            plugin.extraDebug(getTime().getHour() + ":" + getTime().getMinute());
        }

        boolean dayChanged = false;
        boolean weekChanged = false;
        boolean monthChanged = false;
        if (hasDayChanged()) {
            plugin.debug("Day changed");
            dayChanged = true;
        }
        if (hasWeekChanged()) {
            plugin.debug("Week Changed");
            weekChanged = true;
        }
        if (hasMonthChanged()) {
            plugin.debug("Month Changed");
            monthChanged = true;
        }

        if (dayChanged) {
            forceChanged(TimeType.DAY, false);
        }
        if (weekChanged) {
            forceChanged(TimeType.WEEK, false);
        }
        if (monthChanged) {
            forceChanged(TimeType.MONTH, false);
        }
    }

}
