package com.matrixdevelopment.matrixframework.userstore.mysql;

import com.google.common.cache.CacheLoader;
import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.data.ServerData;
import com.matrixdevelopment.matrixframework.usermanagement.UserManager;
import com.matrixdevelopment.matrixframework.userstore.mysql.api.queries.Query;
import com.matrixdevelopment.matrixframework.userstore.sql.Column;
import com.matrixdevelopment.matrixframework.userstore.sql.DataType;
import com.matrixdevelopment.matrixframework.util.misc.CompatibleCacheBuilder;
import com.matrixdevelopment.matrixframework.util.misc.PlayerUtils;
import org.bukkit.configuration.ConfigurationSection;
import com.matrixdevelopment.matrixframework.usermanagement.UUID;

import java.sql.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public class MySQL {

    private com.matrixdevelopment.matrixframework.userstore.mysql.api.MySQL mysql;
    private List<String> columns = Collections.synchronizedList(new ArrayList());

    @Deprecated
    /**
     * @see #table
     */
    private HashMap<String, ArrayList<Column>> oldtable;

    ConcurrentMap<String, ArrayList<Column>> table = CompatibleCacheBuilder.newBuilder().concurrencyLevel(6).build(new CacheLoader<String, ArrayList<Column>>() {

        @Override
        public ArrayList<Column> load(String key) {
            return getExactQuery(new Column("uuid", key, DataType.STRING));
        }
    });

    @Deprecated
    ConcurrentMap<String, ArrayList<Column>> otable = new ConcurrentHashMap<>();

    private ConcurrentLinkedQueue<String> query = new ConcurrentLinkedQueue();
    private String name;
    private Set<String> uuids = Collections.synchronizedSet(new HashSet());
    private Set<String> names = Collections.synchronizedSet(new HashSet());
    private boolean useBatchUpdates = true;
    private int maxSize = 0;
    private Object object1, object2, object3, object4 = new Object();
    private List<String> intColumns;

    public MySQL(String tableName, ConfigurationSection section) {
        intColumns = Collections.synchronizedList(ServerData.getInstance().getIntColumns());

        String tablePrefix = section.getString("prefix");
        String hostName = section.getString("host");
        int port = section.getInt("port");
        String user = section.getString("username");
        String password = section.getString("password");
        String database = section.getString("database");
        long lifetime = section.getLong("maxlifetime", -1);
        int maxThreads = section.getInt("max-connections", 1);

        if (maxThreads < 1) {
            maxThreads = 1;
        }

        boolean useSSL = section.getBoolean("useSSL", false);
        maxSize = section.getInt("max-size", -1);

        if (!section.getString("Name", "").isEmpty()) {
            tableName = section.getString("Name", "");
        }

        if (maxSize >= 0) {
            table = CompatibleCacheBuilder.newBuilder().concurrencyLevel(6).expireAfterAccess(20, TimeUnit.MINUTES)
                    .maximumSize(maxSize).build(new CacheLoader<String, ArrayList<Column>>() {

                        @Override
                        public ArrayList<Column> load(String key) {
                            return getExactQuery(new Column("uuid", key, DataType.STRING));
                        }

                    });
        }

        name = tableName;

        if (tablePrefix != null) {
            name = tablePrefix + tableName;
        }

        mysql = new com.matrixdevelopment.matrixframework.userstore.mysql.api.MySQL(maxThreads);

        if (!mysql.connect(hostName, "" + port, user, password, database, useSSL, lifetime)) {
            MatrixFramework.getInstance().getPlugin().getLogger().warning("Failed to connect MySQL or MariaDB database");
        }

        try {
            Query q = new Query(mysql, "USE " + database + ";");
            q.executeUpdateAsync();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String sql = "CREATE TABLE IF NOT EXISTS " + getName() + " (";
        sql += "uuid VARCHAR(37),";
        sql += "PRIMARY KEY ( uuid )";
        sql += ");";
        Query query;

        try {
            query = new Query(mysql, sql);
            query.executeUpdateAsync();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        loadData();

        alterColumnType("uuid", "VARCHAR(37)");

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                updateBatch();
            }

        }, 10 * 1000, 500);

        MatrixFramework.getInstance().debug("UseBatchUpdates: " + isUseBatchUpdates());

    }

    public void addColumn(String column, DataType dataType) {
        synchronized (object3) {
            String sql = "ALTER TABLE " + getName() + " ADD COLUMN " + column + " text" + ";";

            MatrixFramework.getInstance().debug("Adding column: " + column);

            try {
                Query query = new Query(mysql, sql);
                query.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            getColumns().add(column);

            Column col = new Column(column, dataType);

            for (Map.Entry<String, ArrayList<Column>> entry : table.entrySet()) {
                entry.getValue().add(col);
            }
        }
    }

    public void addToQue(String query) {
        this.query.add(query);
    }

    public void alterColumnType(String column, String newType) {
        checkColumn(column, DataType.STRING);
        MatrixFramework.getInstance().debug("Altering column " + column + " to " + newType);

        if (newType.contains("INT")) {
            addToQue("UPDATE " + getName() + " SET " + column + " = '0' where trim(coalesce(" + column + ", '')) = '';");

            if (!intColumns.contains(column)) {
                intColumns.add(column);
                ServerData.getInstance().setIntColumns(intColumns);
            }
        }

        addToQue("ALTER TABLE " + getName() + " MODIFY " + column + " " + newType + ";");
    }

    public void checkColumn(String column, DataType dataType) {
        synchronized (object4) {
            if (!getColumns().contains(column)) {
                if (!getColumnsQuery().contains(column)) {
                    addColumn(column, dataType);
                }
            }
        }
    }

    public void clearCache() {
        MatrixFramework.getInstance().debug("Clearing cache");
        table.clear();
        clearCacheBasic();
    }

    public void clearCacheBasic() {
        MatrixFramework.getInstance().debug("Clearing cache basic");
        columns.clear();
        columns.addAll(getColumnsQuery());
        uuids.clear();
        uuids.addAll(getUUIDSQuery());
        names.clear();
        names.addAll(getNamesQuery());
    }

    public void close() {
        mysql.disconnect();
    }

    public boolean containsKey(String uuid) {
        return table.containsKey(uuid);
    }

    public boolean containsKeyQuery(String index) {
        String sql = "SELECT uuid FROM " + getName() + ";";

        try {
            Query query = new Query(mysql, sql);
            ResultSet rs = query.executeQuery();

            while (rs.next()) {
                if (rs.getString("uuid").equals(index)) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void deletePlayer(String uuid) {
        String q = "DELETE FROM " + getName() + " WHERE uuid='" + uuid + "';";

        uuids.remove(uuid);
        this.query.add(q);
        removePlayer(uuid);
        names.remove(PlayerUtils.getInstance().getPlayerName(UserManager.getInstance().getUser(new UUID(uuid)), uuid));
        clearCacheBasic();
    }

    public List<String> getColumns() {
        if (columns == null || columns.size() == 0) {
            loadData();
        }

        return columns;
    }

    public ArrayList getColumnsQuery() {
        ArrayList columns = new ArrayList();

        try {
            Query query = new Query(mysql, "SELECT * FROM " + getName() + ";");
            ResultSet rs = query.executeQuery();
            ResultSetMetaData metadata = rs.getMetaData();
            int columncount = 0;

            if (metadata != null) {
                columncount = metadata.getColumnCount();
            }

            for (int i = 1; i <= columncount; i++) {
                String columnName = metadata.getColumnName(i);
                columns.add(columnName);
            }

            return columns;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return columns;
    }

    public ArrayList<Column> getExact(String uuid) {
        loadPlayerIfNeeded(uuid);
        return table.get(uuid);
    }

    public ArrayList<Column> getExactQuery(Column column) {
        ArrayList result = new ArrayList();
        String query = "SELECT * FROM " + getName() + " WHERE `" + column.getName() + "`=?" + ";";

        try {
            Query sql = new Query(mysql, query);
            sql.setParameter(1, column.getValue().toString());
            ResultSet rs = sql.executeQuery();
            rs.next();

            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                String columnName = rs.getMetaData().getColumnLabel(i);
                Column rCol = null;

                if (intColumns.contains(columnName)) {
                    rCol = new Column(columnName, DataType.INT);
                } else {
                    rCol = new Column(columnName, DataType.STRING);
                }

                rCol.setValue(rs.getString(i));
                result.add(rCol);
            }
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {}

        for (String col : getColumns()) {
            result.add(new Column(col, DataType.STRING));
        }
        return result;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public String getName() {
        return name;
    }

    public Set<String> getNames() {
        if (names == null || names.size() == 0) {
            names.clear();
            names.addAll(getNamesQuery());
            return names;
        }
        return names;
    }

    public ArrayList<String> getNamesQuery() {
        ArrayList uuids = new ArrayList();
        ArrayList<Column> rows = getRowsNameQuery();

        if (rows != null) {
            for (Column c : rows) {
                uuids.add(c.getValue().toString());
            }
        }

        return uuids;
    }

    public ArrayList<Column> getRowsNameQuery() {
        ArrayList result = new ArrayList();
        String sql = "SELECT playername FROM " + getName() + ";";

        try {
            Query query = new Query(mysql, sql);
            ResultSet rs = query.executeQuery();

            while (rs.next()) {
                Column rCol = new Column("playername", rs.getString("playername"), DataType.STRING);
                result.add(rCol);
            }
        } catch (SQLException e) {}
        return result;
    }

    public ArrayList<Column> getRowsQuery() {
        ArrayList result = new ArrayList();
        String sql = "SELECT uuid FROM " + getName() + ";";

        try {
            Query query = new Query(mysql, sql);
            ResultSet rs = query.executeQuery();

            while (rs.next()) {
                Column rCol = new Column("uuid", rs.getString("uuid"), DataType.STRING);
                result.add(rCol);
            }
        } catch (SQLException e) {
            return null;
        }
        return result;
    }

    public Set<String> getUUIDS() {
        if (uuids == null || uuids.size() == 0) {
            uuids.clear();
            uuids.addAll(getUUIDSQuery());
            return uuids;
        }
        return uuids;
    }

    public ArrayList<String> getUUIDSQuery() {
        ArrayList uuids = new ArrayList();
        ArrayList<Column> rows = getRowsQuery();

        for (Column c : rows) {
            uuids.add(c.getValue().toString());
        }
        return uuids;
    }

    public void insert(String index, String column, Object value, DataType dataType) {
        insertQuery(index, column, value, dataType);
    }

    public void insertQuery(String index, String column, Object value, DataType dataType) {
        synchronized (object2) {
            String query = "INSERT " + getName() + " ";

            query += "set uuid='" + index + "', ";
            query += column + "='" + value.toString() + "';";

            try {
                uuids.add(index);
                new Query(mysql, query).executeUpdateAsync();
                names.add(PlayerUtils.getInstance().getPlayerName(UserManager.getInstance().getUser(new UUID(index)), index));
            } catch (SQLException e) {
                if (e.getMessage().contains("Duplicate entry")) {
                    MatrixFramework.getInstance().getPlugin().getLogger().severe("Error occurred while inserting user " + index + ", duplicate entry. Enable debug in order to see the error. " + column + ":" + value);
                    MatrixFramework.getInstance().debug(e);
                } else {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean isIntColumn(String key) {
        return intColumns.contains(key);
    }

    public boolean isUseBatchUpdates() {
        return useBatchUpdates;
    }

    public void loadData() {
        columns = getColumnsQuery();

        try {
            useBatchUpdates = mysql.getConnectionManager().getConnection().getMetaData().supportsBatchUpdates();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void loadPlayer(String uuid) {
        table.put(uuid, getExactQuery(new Column("uuid", uuid, DataType.STRING)));
    }

    public void loadPlayerIfNeeded(String uuid) {
        if (!containsKey(uuid)) {
            synchronized (object1) {
                loadPlayer(uuid);
            }
        }
    }

    public void removePlayer(String uuid) {
        table.remove(uuid);
    }

    public void update(String index, String column, Object value, DataType dataType) {
        if (value == null) {
            MatrixFramework.getInstance().extraDebug("MySQL value null: " + column);
            return;
        }

        checkColumn(column, dataType);

        if (getUUIDS().contains(index)) {
            synchronized (object2) {
                for (Column col : getExact(index)) {
                    if (col.getName().equals(column)) {
                        col.setValue(value);
                    }
                }

                String query = "UPDATE " + getName() + " SET ";

                if (dataType == DataType.STRING) {
                    query += "`" + column + "`='" + value.toString() + "'";
                } else {
                    query += "`" + column + "`=" + value;
                }

                query += " WHERE `uuid`=";
                query += "'" + index + "';";

                addToQue(query);
            }
        } else {
            insert(index, column, value, dataType);
        }
    }

    public void updateBatch() {
        if (query.size() > 0) {
            MatrixFramework.getInstance().extraDebug("Query size: " + query.size());
            String sql = "";

            while (query.size() > 0) {
                String text = query.poll();

                if (!text.endsWith(";")) {
                    text += ";";
                }
                sql += text;
            }

            try {
                if (useBatchUpdates) {
                    Connection conn = mysql.getConnectionManager().getConnection();
                    Statement st = conn.createStatement();

                    for (String str : sql.split(";")) {
                        st.addBatch(str);
                    }

                    st.executeBatch();
                    st.close();
                    conn.close();
                } else {
                    for (String text : sql.split(";")) {
                        try {
                            Query query = new Query(mysql, text);
                            query.executeUpdateAsync();
                        } catch (SQLException e) {
                            MatrixFramework.getInstance().getPlugin().getLogger().severe("Error occurred while executing sql: " + e.toString() + ", enable debug to see full stacktraces");
                            MatrixFramework.getInstance().debug(e);
                        }
                    }
                }
            } catch (SQLException e) {
                MatrixFramework.getInstance().extraDebug("Failed to send query: " + sql);
                e.printStackTrace();
            }
        }
    }

}
