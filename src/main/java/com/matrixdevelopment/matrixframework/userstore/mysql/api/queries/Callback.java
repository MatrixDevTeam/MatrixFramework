package com.matrixdevelopment.matrixframework.userstore.mysql.api.queries;

public interface Callback<V extends Object, T extends Throwable> {

    void call(V result, T thrown);

}
