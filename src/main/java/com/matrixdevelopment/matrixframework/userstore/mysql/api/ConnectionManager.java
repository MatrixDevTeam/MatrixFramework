package com.matrixdevelopment.matrixframework.userstore.mysql.api;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionManager {

    private HikariDataSource dataSource;
    private String host, port, username, password, database;
    private int connectionTimeout, maximumPoolSize;
    private boolean useSSL = false;
    private long maxLifetimeMS;

    public ConnectionManager(String host, String port, String username, String password, String database) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
        connectionTimeout = 30000;
        maximumPoolSize = 5;
    }

    public ConnectionManager(String host, String port, String username, String password, String database, int maxConnections, boolean useSSL, long lifeTime) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
        connectionTimeout = 30000;

        if (maxConnections > 5) {
            maximumPoolSize = maxConnections;
        } else {
            maximumPoolSize = 5;
        }

        this.useSSL = useSSL;
        this.maxLifetimeMS = lifeTime;
    }

    public ConnectionManager(String host, String port, String username, String password, String database,
                             int connectionTimeout, int maximumPoolSize) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
        this.connectionTimeout = connectionTimeout;
        this.maximumPoolSize = maximumPoolSize;
    }

    public void close() {
        if (isClosed()) {
            throw new IllegalStateException("Connection is not open.");
        }
        dataSource.close();
    }

    public Connection getConnection() {
        try {
            if (isClosed()) {
                MatrixFramework.getInstance().debug("Connection closed... opening....");
                open();
            }
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return the connectionTimeout
     */
    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    /**
     * @return the database
     */
    public String getDatabase() {
        return database;
    }

    /**
     * @return the dataSource
     */
    public HikariDataSource getDataSource() {
        return dataSource;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    public boolean isClosed() {
        return dataSource == null || dataSource.isClosed();
    }

    /**
     * @return the useSSL
     */
    public boolean isUseSSL() {
        return useSSL;
    }

    public boolean open() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            HikariConfig config = new HikariConfig();
            config.setDriverClassName("com.mysql.jdbc.Driver");
            config.setUsername(username);
            config.setPassword(password);
            config.setJdbcUrl(String.format("jdbc:mysql://%s:%s/%s", host, port, database) + "?useSSL=" + useSSL + "&allowMultiQueries=true&rewriteBatchedStatements=true&useDynamicCharsetInfo=false");
            config.setConnectionTimeout(connectionTimeout);
            config.setMaximumPoolSize(maximumPoolSize);
            config.setMinimumIdle(maximumPoolSize);

            if (maxLifetimeMS > -1) {
                config.setMaxLifetime(maxLifetimeMS);
            }

            config.addDataSourceProperty("cachePrepStmts", true);
            config.addDataSourceProperty("prepStmtCacheSize", 250);
            config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
            config.addDataSourceProperty("useServerPrepStmts", true);
            dataSource = new HikariDataSource(config);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @param connectionTimeout
     *            the connectionTimeout to set
     */
    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    /**
     * @param database
     *            the database to set
     */
    public void setDatabase(String database) {
        this.database = database;
    }

    /**
     * @param dataSource
     *            the dataSource to set
     */
    public void setDataSource(HikariDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @param host
     *            the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @param maximumPoolSize
     *            the maximumPoolsize to set
     */
    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @param port
     *            the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @param useSSL
     *            the useSSL to set
     */
    public void setUseSSL(boolean useSSL) {
        this.useSSL = useSSL;
    }

}
