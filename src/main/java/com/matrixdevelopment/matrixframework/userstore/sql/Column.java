package com.matrixdevelopment.matrixframework.userstore.sql;

public class Column {

    public String name;
    public DataType dataType;
    public int limit = 0;
    private Object val;

    public Column(String name, DataType dataType) {
        this.dataType = dataType;
        this.name = name;
        limit = 0;
    }

    public Column(String name, DataType dataType, int limit) {
        this.name = name;
        this.dataType = dataType;
        this.limit = limit;
    }

    public Column(String name, Object val, DataType dataType) {
        this.name = name;
        this.dataType = dataType;
        limit = 0;
        this.val = val;
    }

    public DataType getType() {
        return dataType;
    }

    public int getLimit() {
        return limit;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return val;
    }

    public void setType(DataType dataType) {
        this.dataType = dataType;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(Object val) {
        this.val = val;
    }
}
