package com.matrixdevelopment.matrixframework.userstore.sql.db;

import org.bukkit.plugin.Plugin;

import java.util.logging.Level;

public class Error {

    public static void close(Plugin plugin, Exception e) {
        plugin.getLogger().log(Level.SEVERE, "Failed to close MySQL connection, so here's the error: ", e);
    }

    public static void execute(Plugin plugin, Exception e) {
        plugin.getLogger().log(Level.SEVERE, "Could not execute MySQL statement: ", e);
    }

}
