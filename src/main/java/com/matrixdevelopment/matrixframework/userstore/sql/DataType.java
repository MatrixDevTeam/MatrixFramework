package com.matrixdevelopment.matrixframework.userstore.sql;

public enum DataType {

    STRING,
    INT,
    FLOAT

}
