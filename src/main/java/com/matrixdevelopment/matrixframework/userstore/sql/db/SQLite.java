package com.matrixdevelopment.matrixframework.userstore.sql.db;

import com.matrixdevelopment.matrixframework.userstore.sql.Database;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.logging.Level;

public class SQLite {

    private String dbName, query, dir;
    private boolean inDF = true;
    private Plugin plugin;
    private Connection connection;

    public SQLite(Plugin plugin, String dbName, Database db) {
        this.plugin = plugin;
        this.dbName = dbName;
        query = db.getTableQuery();
        connection = getSQLConnection();
    }

    public SQLite(Plugin plugin, String dbName, Database db, String directory) {
        this.plugin = plugin;
        this.dbName = dbName;
        query = db.getTableQuery();
        inDF = false;
        dir = directory;
        connection = getSQLConnection();
    }

    public void close(PreparedStatement ps, ResultSet rs) {
        try {
            if (ps != null) {
                ps.close();
            } else if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            Error.close(plugin, e);
        }
    }

    public void closeConnection() {
        try {
            if (!connection.isClosed()) {
                connection.close();
                connection = null;
            } else {
                connection = null;
            }
        } catch (Exception e) {
            Error.close(plugin, e);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public String getDBName() {
        return dbName;
    }

    public String getDirectory() {
        return dir;
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public String getQuery() {
        return query;
    }

    public Connection getSQLConnection() {
        File file;

        if (inDF) {
            file = new File(plugin.getDataFolder(), dbName + ".db");
            plugin.getDataFolder().mkdirs();
        } else {
            new File(dir).mkdirs();
            file = new File(dir + "/" + dbName + ".db");
        }

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().severe("File creation error for file: " + dbName + ".db");
            }
        }

        try {
            if (connection != null && !connection.isClosed()) {
                return connection;
            }

            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + file);
            return connection;
        } catch (SQLException e) {
            plugin.getLogger().log(Level.SEVERE, "SQLite exception on initialization.", e);
        } catch (ClassNotFoundException e) {
            plugin.getLogger().severe("SQLite JBDC library not found!");
        }
        return null;
    }

    public boolean isInDF() {
        return inDF;
    }

    public void load() {
        connection = getSQLConnection();

        try {
            PreparedStatement s = connection.prepareStatement(query);
            s.executeUpdate();
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
