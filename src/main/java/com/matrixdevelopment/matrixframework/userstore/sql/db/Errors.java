package com.matrixdevelopment.matrixframework.userstore.sql.db;

public class Errors {

    public static String noSQLConn() {
        return "Unable to retrieve MySQL connection: ";
    }

    public static String noTableFound() {
        return "Database Error: No table found!";
    }

    public static String sqlConnClose() {
        return "Failed to close MySQL connection: ";
    }

    public static String sqlConnExecute() {
        return "Could not execute MySQL statement: ";
    }

}
