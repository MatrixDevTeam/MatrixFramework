package com.matrixdevelopment.matrixframework.commands.gui;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.rewards.RewardHandler;
import com.matrixdevelopment.matrixframework.util.inv.BInventory;
import com.matrixdevelopment.matrixframework.util.inv.BInventoryButton;
import com.matrixdevelopment.matrixframework.util.item.ItemBuilder;
import com.matrixdevelopment.matrixframework.util.value.ValueRequest;
import com.matrixdevelopment.matrixframework.util.value.listeners.StringListener;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class AdminGUI {

    static AdminGUI INSTANCE = new AdminGUI();

    public static AdminGUI getInstance() {
        return INSTANCE;
    }

    MatrixFramework plugin = MatrixFramework.getInstance();
    private ArrayList<BInventoryButton> pluginGUIs;

    private AdminGUI() {}

    public void addButton(BInventoryButton b) {
        if (pluginGUIs == null) {
            pluginGUIs = new ArrayList<>();
        }

        pluginGUIs.add(b);
    }

    public void openGUI(Player player) {
        if (!player.hasPermission(MatrixFramework.getInstance().getPermPrefix() + ".admin-edit")) {
            player.sendMessage("Insufficient Permissions");
            return;
        }

        BInventory inv = new BInventory("AdminGUI");

        inv.addButton(inv.getNextSlot(), new BInventoryButton("&cRewards", new String[]{"&cMiddle click to create"}, new ItemStack(Material.DIAMOND)) {
            @Override
            public void onClick(BInventory.ClickEvent e) {
                Player player = e.getWhoClicked();
                if (e.getClick().equals(ClickType.MIDDLE)) {
                    new ValueRequest().requestString(player, new StringListener() {
                        @Override
                        public void onInput(Player player, String value) {
                            RewardHandler.getInstance().getReward(value);
                            player.sendMessage("Reward file created!");
                            plugin.reload();
                        }
                    });
                } else {
                    RewardEditGUI.getInstance().openRewardsGUI(player);
                }
            }
        });

        inv.addButton(inv.getNextSlot(), new BInventoryButton(new ItemBuilder(Material.PLAYER_HEAD, 1).setName("&cUsers")) {
            @Override
            public void onClick(BInventory.ClickEvent e) {
                Player player = e.getWhoClicked();
                UserGUI.getInstance().openUsersGUI(player);
            }
        });


        if (pluginGUIs != null) {
            for (BInventoryButton b : pluginGUIs) {
                inv.addButton(inv.getNextSlot(), b);
            }
        }

        inv.openInventory(player);
    }

}
