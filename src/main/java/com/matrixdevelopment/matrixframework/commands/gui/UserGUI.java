package com.matrixdevelopment.matrixframework.commands.gui;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.rewards.Reward;
import com.matrixdevelopment.matrixframework.rewards.RewardHandler;
import com.matrixdevelopment.matrixframework.rewards.RewardOptions;
import com.matrixdevelopment.matrixframework.usermanagement.User;
import com.matrixdevelopment.matrixframework.usermanagement.UserManager;
import com.matrixdevelopment.matrixframework.util.inv.BInventory;
import com.matrixdevelopment.matrixframework.util.inv.BInventoryButton;
import com.matrixdevelopment.matrixframework.util.item.ItemBuilder;
import com.matrixdevelopment.matrixframework.util.misc.ArrayUtils;
import com.matrixdevelopment.matrixframework.util.misc.PlayerUtils;
import com.matrixdevelopment.matrixframework.util.value.ValueRequest;
import com.matrixdevelopment.matrixframework.util.value.ValueRequestBuilder;
import com.matrixdevelopment.matrixframework.util.value.listeners.StringListener;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;

public class UserGUI {

    static UserGUI INSTANCE = new UserGUI();

    public static UserGUI getInstance() {
        return INSTANCE;
    }

    MatrixFramework plugin = MatrixFramework.getInstance();

    private HashMap<Plugin, BInventoryButton> extraButtons = new HashMap<>();

    private UserGUI() {}

    public synchronized void addPluginButton(Plugin plugin, BInventoryButton inv) {
        extraButtons.put(plugin, inv);
    }

    public String getCurrentPlayer(Player player) {
        return (String) PlayerUtils.getInstance().getPlayerMeta(player, "usergui");
    }

    public void openUserGUI(Player player, final String playerName) {
        if (!player.hasPermission("MatrixFramework.UserEdit")) {
            player.sendMessage("Not enough permissions");
            return;
        }
        BInventory inv = new BInventory("UserGUI: " + playerName);
        inv.addButton(new BInventoryButton("Give Reward File", new String[] {}, new ItemStack(Material.STONE)) {

            @Override
            public void onClick(BInventory.ClickEvent clickEvent) {
                ArrayList<String> rewards = new ArrayList<>();
                for (Reward reward : RewardHandler.getInstance().getRewards()) {
                    rewards.add(reward.getRewardName());
                }

                new ValueRequest().requestString(clickEvent.getPlayer(), "", ArrayUtils.getInstance().convert(rewards), true, new StringListener() {

                            @Override
                            public void onInput(Player player, String value) {
                                User user = UserManager.getInstance()
                                        .getUser(UserGUI.getInstance().getCurrentPlayer(player));
                                RewardHandler.getInstance().giveReward(user, value, new RewardOptions());
                                player.sendMessage("Given " + user.getPlayerName() + " reward file " + value);

                            }
                });
            }
        });

        inv.addButton(new BInventoryButton(new ItemBuilder(Material.WRITABLE_BOOK).setName("Edit Data")) {

            @Override
            public void onClick(BInventory.ClickEvent clickEvent) {
                Player player = clickEvent.getPlayer();
                BInventory inv = new BInventory("Edit Data, click to change");
                final User user = UserManager.getInstance().getUser(playerName);
                for (final String key : user.getData().getKeys()) {
                    String value = user.getData().getString(key);
                    inv.addButton(new BInventoryButton(new ItemBuilder(Material.STONE).setName(key + " = " + value)) {

                        @Override
                        public void onClick(BInventory.ClickEvent clickEvent) {
                            new ValueRequestBuilder(new StringListener() {

                                @Override
                                public void onInput(Player player, String newValue) {
                                    user.getData().setString(key, newValue);
                                    user.getData().updatePlayerData();
                                    openUserGUI(player, playerName);
                                }
                            }, new String[] {}).allowCustomOption(true).currentValue(value).request(player);
                        }
                    });
                }

                inv.openInventory(player);
            }
        });

        inv.addButton(new BInventoryButton(new ItemBuilder(Material.PAPER).setName("&cView player data")) {

            @Override
            public void onClick(BInventory.ClickEvent clickEvent) {
                Player player = clickEvent.getPlayer();
                User user = UserManager.getInstance().getUser(player);
                for (String key : user.getData().getKeys()) {
                    user.sendMessage("&c&l" + key + " &c" + user.getData().getString(key));
                }
            }
        });

        for (BInventoryButton button : extraButtons.values()) {
            inv.addButton(button);
        }

        inv.openInventory(player);
    }

    public void openUsersGUI(Player player) {
        if (!player.hasPermission("MatrixFramework.useredit")) {
            player.sendMessage("Not enough permissions");
            return;
        }

        ArrayList<String> players = new ArrayList<String>();
        for (Player p : Bukkit.getOnlinePlayers()) {
            players.add(p.getName());
        }
        new ValueRequest().requestString(player, "", ArrayUtils.getInstance().convert(players), true, new StringListener() {

            @Override
            public void onInput(Player player, String value) {
                setCurrentPlayer(player, value);
                openUserGUI(player, value);
            }
        });
    }

    private void setCurrentPlayer(Player player, String playerName) {
        PlayerUtils.getInstance().setPlayerMeta(player, "usergui", playerName);
    }

}
