package com.matrixdevelopment.matrixframework.commands.executor;

import com.matrixdevelopment.matrixframework.commandapi.CommandHandler;
import com.matrixdevelopment.matrixframework.commands.CommandLoader;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

import java.util.ArrayList;

public class ValueRequestInputCommand extends BukkitCommand {

    public ValueRequestInputCommand(String name) {
        super(name);
        description = "ValueRequestInput";
        setAliases(new ArrayList());
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        for (CommandHandler cmd : CommandLoader.getInstance().getValueRequestCommands()) {
            if (cmd.runCommand(sender, args)) {
                return true;
            }
        }

        return true;
    }

}
