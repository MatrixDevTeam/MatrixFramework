package com.matrixdevelopment.matrixframework.commands.tabcomplete;

import com.matrixdevelopment.matrixframework.util.misc.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.*;

public class TabCompleter implements org.bukkit.command.TabCompleter {

    public ArrayList<String> getTabCompleteOptions(CommandSender sender, String[] args, int argNum) {
        ArrayList<String> cmds = new ArrayList();

        Collections.sort(cmds, String.CASE_INSENSITIVE_ORDER);
        return cmds;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        ArrayList<String> tab = new ArrayList<>();
        Set<String> cmds = new HashSet<>();

        for (String str : cmds) {
            if (StringUtils.getInstance().startsWithIgnoreCase(str, args[args.length - 1])) {
                tab.add(str);
            }
        }

        Collections.sort(tab, String.CASE_INSENSITIVE_ORDER);
        return tab;
    }

}
