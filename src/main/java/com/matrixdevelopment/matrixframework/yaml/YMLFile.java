package com.matrixdevelopment.matrixframework.yaml;

import com.matrixdevelopment.matrixframework.util.files.FilesManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public abstract class YMLFile {

    private FileConfiguration data;
    private File dFile;

    /**
     * Instantiates a new YML file.
     *
     * @param file the File
     */
    public YMLFile(File file) {
        dFile = file;
    }

    /**
     * Instantiates a new YML file.
     *
     * @param file the file
     * @param setup setup the File in location
     */
    public YMLFile(File file, boolean setup) {
        dFile = file;

        if (setup) {
            setup();
        }
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public FileConfiguration getData() {
        return data;
    }

    /**
     * Gets the Data File
     *
     * @return the dFile (Data File)
     */
    public File getDFile() {
        return dFile;
    }

    public abstract void onFileCreation();

    /**
     * Reload the data files
     */
    public void reload() {
        data = YamlConfiguration.loadConfiguration(dFile);
    }

    /**
     * Save the data files
     */
    public void saveData() {
        FilesManager.getInstance().editFile(dFile, data);
    }

    public void setData(FileConfiguration data) {
        Map<String, Object> map = data.getConfigurationSection("").getValues(true);

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            this.data.set(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Setup
     */
    public void setup() {
        getDFile().getParentFile().mkdirs();

        if (!dFile.exists()) {
            try {
                getDFile().createNewFile();
                onFileCreation();
            } catch (IOException e) {
                Bukkit.getServer().getLogger().severe("Could not create " + getDFile().getName() + "!");
            }
        }
        data = YamlConfiguration.loadConfiguration(dFile);
    }
}
