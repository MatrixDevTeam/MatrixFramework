package com.matrixdevelopment.matrixframework.backups;

import com.matrixdevelopment.matrixframework.MatrixFramework;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipCreator {

    static ZipCreator INSTANCE = new ZipCreator();

    public static ZipCreator getInstance() {
        return INSTANCE;
    }

    MatrixFramework plugin = MatrixFramework.getInstance();

    private ZipCreator() {}

    /**
     * Adds the all files.
     *
     * @param dir the Directory
     * @param fileList the file list
     */
    private void addAllFiles(File dir, List<File> fileList) {
        try {
            File[] files = dir.listFiles();

            for (File file : files) {
                fileList.add(file);

                if (file.isDirectory()) {
                    plugin.debug("Directory: " + file.getCanonicalPath());

                    if (!file.getAbsolutePath().contains(MatrixFramework.getInstance().getPlugin().getName() + File.separator + "backups") && !file.getAbsolutePath().contains(MatrixFramework.getInstance().getPlugin().getName() + File.separator + "reports")) {
                            addAllFiles(file, fileList);
                    }
                } else {
                    plugin.debug("File: " + file.getCanonicalPath());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds files the to zip.
     *
     * @param file the File
     * @param zos the Zip Output Stream
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void addToZip(File file, ZipOutputStream zos) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(file);
        String zipFilePath = file.getPath();

        plugin.extraDebug("Writing '" + zipFilePath + "' to zip file");

        ZipEntry zipEntry = new ZipEntry(zipFilePath);
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;

        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }

    public void createReport() {
        long time = Calendar.getInstance().getTime().getTime();
        create(plugin.getPlugin().getDataFolder(), new File(plugin.getPlugin().getDataFolder(), "reports" + File.separator + "Report." + Long.toString(time) + ".zip"));
    }

    public void create(File directory, File zipLocation) {

        if (zipLocation.exists()) {
            zipLocation.delete();
        }

        try {
            zipLocation.getParentFile().mkdirs();
            zipLocation.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<File> fileList = new ArrayList();

        try {
            plugin.debug("<--> Getting references to all files in: " +directory.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        addAllFiles(directory, fileList);
        plugin.debug("<--> Creating ZIP File");

        writeZipFile(fileList, zipLocation);
        plugin.debug("<--> Done!");
    }

    private void writeZipFile(List<File> fileList, File zipFile) {

        try {
            File fileZipFolder = new File(plugin.getPlugin().getDataFolder().getAbsolutePath() + File.separator + "reports");

            if (!fileZipFolder.exists()) {
                fileZipFolder.exists();
            }

            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);

            for (File file : fileList) {
                if (!file.isDirectory()) {
                    if (!file.getAbsolutePath().equals(zipFile.getAbsolutePath())) {
                        addToZip(file, zos);
                    }
                }
            }

            plugin.getPlugin().getLogger().info("Created zip file at " + zipFile.getAbsolutePath());

            zos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
