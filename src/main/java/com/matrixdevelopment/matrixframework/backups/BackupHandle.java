package com.matrixdevelopment.matrixframework.backups;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.time.TimeChecker;
import com.matrixdevelopment.matrixframework.time.TimeType;
import com.matrixdevelopment.matrixframework.time.events.DateChangeEvent;
import com.matrixdevelopment.matrixframework.util.misc.MiscUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.io.File;
import java.time.LocalDateTime;

public class BackupHandle implements Listener {

    private static BackupHandle INSTANCE = new BackupHandle();

    public static BackupHandle getInstance() {
        return INSTANCE;
    }

    public BackupHandle() {}

    public void checkOldBackups() {
        for (File file : new File(MatrixFramework.getInstance().getPlugin().getDataFolder(), "backups").listFiles()) {
            long lastModified = file.lastModified();

            if (LocalDateTime.now().minusDays(5).isAfter(MiscUtil.getInstance().getTime(lastModified))) {
                file.delete();
                MatrixFramework.getInstance().debug("Deleting old backup: " + file.getName());
            }
        }
    }

    @EventHandler
    public void onDateChange(DateChangeEvent e) {
        if (!e.getTimeType().equals(TimeType.DAY)) {
            return;
        }

        if (!MatrixFramework.getInstance().isCreateBackups()) {
            return;
        }

        LocalDateTime now = TimeChecker.getInstance().getTime();
        ZipCreator.getInstance().create(MatrixFramework.getInstance().getPlugin().getDataFolder(), new File(MatrixFramework.getInstance().getPlugin().getDataFolder(), "backups" + File.separator + "backup-" + now.getYear() + "_" + now.getYear() + "_" + now.getDayOfMonth() + ".zip"));
        checkOldBackups();
    }

}
