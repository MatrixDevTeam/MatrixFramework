package com.matrixdevelopment.matrixframework.util.files;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.thread.FileThread;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;

public class FilesManager {

    static FilesManager INSTANCE = new FilesManager();

    /**
     * Gets the single instance of FilesManager.
     *
     * @return single instance of FilesManager
     */
    public static FilesManager getInstance() {
        return INSTANCE;
    }

    MatrixFramework plugin = MatrixFramework.getInstance();

    /**
     * Instantiates a new files manager.
     */
    private FilesManager() {}

    /**
     * Edits the file.
     *
     * @param file the File
     * @param data the Data
     */
    public void editFile(File file, FileConfiguration data) {
        FileThread.getInstance().run(new Runnable() {

            @Override
            public void run() {
                try {
                    data.save(file);
                } catch (IOException e) {
                    Bukkit.getServer().getLogger().severe("Could not save " + file.getName());
                }
            }
        });
    }

}
