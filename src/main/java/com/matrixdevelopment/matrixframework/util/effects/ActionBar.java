package com.matrixdevelopment.matrixframework.util.effects;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.util.misc.StringUtils;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class ActionBar {

    MatrixFramework plugin = MatrixFramework.getInstance();
    private int duration;
    private String msg;

    public ActionBar(String msg, int duration) {
        setMSG(StringUtils.getInstance().colorize(msg));
        setDuration(duration);
    }

    public int getDuration() {
        return duration;
    }

    public String getMSG() {
        return msg;
    }

    public void send(Player... players) {
        for (Player player : players) {
            sendActionBar(player, getMSG(), getDuration());
        }
    }

    /**
     * Send actionbar
     *
     * @param player the Player
     * @param message the Message
     */
    public void sendActionBar(Player player, String message) {
        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(message));
    }

    /**
     * Send actionbar
     *
     * @param player the Player
     * @param message the Message
     * @param duration the duration
     */
    public void sendActionBar(final Player player, final String message, int duration) {
        sendActionBar(player, message);

        if (duration >= 0) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    sendActionBar(player, "");
                }
            }.runTaskLater(plugin.getPlugin(), duration + 1);
        }

        while (duration > 60) {
            duration -= 60;
            int sched = duration % 60;

            new BukkitRunnable() {
                @Override
                public void run() {
                    sendActionBar(player, message);
                }
            }.runTaskLater(plugin.getPlugin(), sched);
        }
    }

    public void sendActionBarToAllPlayers(String message) {
        sendActionBarToAllPlayers(message, -1);
    }

    /**
     * Send actionbar to all players
     *
     * @param message the Message
     * @param duration the duration
     */
    public void sendActionBarToAllPlayers(String message, int duration) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            sendActionBar(p, message, duration);
        }
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setMSG(String msg) {
        this.msg = msg;
    }

}
