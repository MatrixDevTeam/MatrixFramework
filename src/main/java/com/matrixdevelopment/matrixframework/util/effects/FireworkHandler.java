package com.matrixdevelopment.matrixframework.util.effects;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FireworkExplodeEvent;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

public class FireworkHandler implements Listener {

    static FireworkHandler INSANCE = new FireworkHandler();

    public static FireworkHandler getInstance() {
        return INSANCE;
    }

    private ConcurrentLinkedQueue<Firework> fireworks = new ConcurrentLinkedQueue<>();
    MatrixFramework plugin = MatrixFramework.getInstance();

    private FireworkHandler() {}

    public void launchFirework(Location loc, int power, ArrayList<String> colors, ArrayList<String> fadeOutColor, boolean trail, boolean flicker, ArrayList<String> types) {
        Bukkit.getScheduler().runTask(plugin.getPlugin(), new Runnable() {
            @Override
            public void run() {
                Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
                FireworkMeta fwmeta = fw.getFireworkMeta();
                FireworkEffect.Builder builder = FireworkEffect.builder();

                if (trail) {
                    builder.withTrail();
                }

                if (flicker) {
                    builder.withFlicker();
                }

                for (String color : colors) {
                    try {
                        builder.withColor(DyeColor.valueOf(color).getColor());
                    } catch (Exception e) {
                        plugin.getPlugin().getLogger().info(color + " is not a valid color, please refer to https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Color.html");
                    }
                }

                for (String color : fadeOutColor) {
                    try {
                        builder.withFade(DyeColor.valueOf(color).getColor());
                    } catch (Exception e) {
                        plugin.getPlugin().getLogger().info(color + " is not a valid color, please refer to https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Color.html");
                    }
                }

                for (String type : types) {
                    try {
                        builder.with(FireworkEffect.Type.valueOf(type));
                    } catch (Exception e) {
                        plugin.getPlugin().getLogger().info(type + " is not a valid Firework Effect, please refer to https://hub.spigotmc.org/javadocs/spigot/org/bukkit/FireworkEffect.Type.html");
                    }
                }

                fwmeta.addEffects(builder.build());
                fwmeta.setPower(power);
                fw.setFireworkMeta(fwmeta);
                fireworks.add(fw);
            }
        });
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onFireworkDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Firework && e.getEntity() instanceof Player) {
            Firework fw = (Firework) e.getDamager();

            if (fireworks.contains(fw)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onFireworkExplode(FireworkExplodeEvent e) {
        if (e.getEntity() instanceof Firework) {
            Firework fw = e.getEntity();

            if (fireworks.contains(fw)) {
                Bukkit.getScheduler().runTaskLaterAsynchronously(plugin.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        fireworks.remove(fw);
                    }
                }, 101);
            }
        }
    }



}
