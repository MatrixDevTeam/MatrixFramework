package com.matrixdevelopment.matrixframework.util.animation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

class AnimationAPI {

    private static AnimationAPI INSTANCE = new AnimationAPI();

    public static AnimationAPI getInstance() {
        return INSTANCE;
    }

    private AnimationAPI() {}

    /**
     * Run an animation
     *
     * @param texts Text for animation
     * @param timeBetween Time between text updates
     * @param animation Method to run to display text
     */
    public void runAnimation(ArrayList<String> texts, long timeBetween, final Animation animation) {
        Iterator<String> it = texts.iterator();

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                if (it.hasNext()) {
                    animation.onAnimate(it.next());
                } else {
                    cancel();
                }
            }

        }, 0, timeBetween);
    }

}
