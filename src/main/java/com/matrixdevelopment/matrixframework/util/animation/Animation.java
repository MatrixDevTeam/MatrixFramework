package com.matrixdevelopment.matrixframework.util.animation;

public interface Animation {

    void onAnimate(String text);

}
