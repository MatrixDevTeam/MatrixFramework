package com.matrixdevelopment.matrixframework.util.book;

import org.bukkit.entity.Player;

public abstract class BookSign {

    /**
     * On book sign
     *
     * @param player the Player
     * @param input the Input
     */
    public abstract void onBookSign(Player player, String input);

}
