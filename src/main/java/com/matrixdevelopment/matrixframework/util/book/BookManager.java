package com.matrixdevelopment.matrixframework.util.book;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.usermanagement.User;
import com.matrixdevelopment.matrixframework.usermanagement.UserManager;
import com.matrixdevelopment.matrixframework.util.misc.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.inventory.ItemStack;

public class BookManager implements Listener {

    public Listener listener;

    public BookManager(Player player, String start, BookSign listener) {
        User user = UserManager.getInstance().getUser(player);
        ItemStack item = new ItemStack(Material.WRITABLE_BOOK);
        PlayerUtils.getInstance().setPlayerMeta(player, "BookManager", listener);

        user.giveItem(item);

        this.listener = new Listener() {
            @EventHandler
            public void bookEdit(PlayerEditBookEvent e) {
                Player player = e.getPlayer();
                boolean destroy = false;
                String st = "";

                for (String str : e.getNewBookMeta().getPages()) {
                    st += str;
                }

                final String input = st;

                BookSign listener = (BookSign) PlayerUtils.getInstance().getPlayerMeta(player, "BookManager");

                Bukkit.getScheduler().runTaskAsynchronously(MatrixFramework.getInstance().getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        listener.onBookSign(player, input);
                    }
                });

                player.getInventory().getItem(e.getSlot()).setType(Material.AIR);
                player.getInventory().setItem(e.getSlot(), new ItemStack(Material.AIR));
                destroy = true;

                if (destroy) {
                    destroy();
                }
            }
        };

        Bukkit.getPluginManager().registerEvents(this.listener, MatrixFramework.getInstance().getPlugin());
    }

    public void destroy() {
        HandlerList.unregisterAll(listener);
    }

}
