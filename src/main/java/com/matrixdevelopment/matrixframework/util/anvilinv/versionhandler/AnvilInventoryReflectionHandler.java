package com.matrixdevelopment.matrixframework.util.anvilinv.versionhandler;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.nms.NMSManager;
import com.matrixdevelopment.matrixframework.util.anvilinv.AnvilInventory;
import com.matrixdevelopment.matrixframework.util.misc.PlayerUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;

public class AnvilInventoryReflectionHandler implements AnvilInventoryVersionHandler {

    private static Class<?> BlockPosition;
    private static Class<?> PacketPlayOutOpenWindow;
    private static Class<?> ContainerAnvil;
    private static Class<?> ChatMessage;
    private static Class<?> EntityHuman;
    private Inventory inv;

    public AnvilInventoryReflectionHandler(final Player player, final AnvilInventory.AnvilClickEventHandler anvilClickEventHandler) {
        loadClasses();
        PlayerUtils.getInstance().setPlayerMeta(player, "AnvilInventory", anvilClickEventHandler);
    }

    /**
     * (non-Javadoc)
     *
     * @see AnvilInventoryVersionHandler#getInventory()
     */
    @Override
    public Inventory getInventory() {
        return inv;
    }

    /**
     * (non-Javadoc)
     *
     * @see AnvilInventoryVersionHandler#loadClasses()
     */
    @Override
    public void loadClasses() {
        BlockPosition = NMSManager.get().getNMSClass("BlockPosition");
        PacketPlayOutOpenWindow = NMSManager.get().getNMSClass("PacketPlayOutOpenWindow");
        ContainerAnvil = NMSManager.get().getNMSClass("ContainerAnvil");
        EntityHuman = NMSManager.get().getNMSClass("EntityHuman");
        ChatMessage = NMSManager.get().getNMSClass("ChatMessage");
    }

    /**
     * (non-Javadoc)
     *
     * @see AnvilInventoryVersionHandler#open(Player, HashMap)
     */
    @Override
    public void open(Player player, HashMap<AnvilInventory.AnvilSlot, ItemStack> items) {
        player.setLevel(player.getLevel() + 1);

        try {
            Object p = NMSManager.get().getHandle(player);

            Object container = ContainerAnvil
                    .getConstructor(NMSManager.get().getNMSClass("PlayerInventory"),
                            NMSManager.get().getNMSClass("World"), BlockPosition, EntityHuman)
                    .newInstance(NMSManager.get().getPlayerField(player, "inventory"),
                            NMSManager.get().getPlayerField(player, "world"),
                            BlockPosition.getConstructor(int.class, int.class, int.class).newInstance(0, 0, 0), p);
            NMSManager.get().getField(NMSManager.get().getNMSClass("Container"), "checkReachable").set(container,
                    false);

            // Set the items to the items from the inventory given
            Object bukkitView = NMSManager.get().invokeMethod("getBukkitView", container);
            inv = (Inventory) NMSManager.get().invokeMethod("getTopInventory", bukkitView);

            for (AnvilInventory.AnvilSlot slot : items.keySet()) {
                inv.setItem(slot.getSlot(), items.get(slot));
            }

            // Counter stuff that the game uses to keep track of inventories
            int c = (int) NMSManager.get().invokeMethod("nextContainerCounter", p);

            // Send the packet
            Constructor<?> chatMessageConstructor = ChatMessage.getConstructor(String.class, Object[].class);
            Object playerConnection = NMSManager.get().getPlayerField(player, "playerConnection");
            Object packet = PacketPlayOutOpenWindow.getConstructor(int.class, String.class,
                    NMSManager.get().getNMSClass("IChatBaseComponent"), int.class).newInstance(c, "minecraft:anvil",
                    chatMessageConstructor.newInstance("Repairing", new Object[] {}), 0);

            Method sendPacket = NMSManager.get().getMethod("sendPacket", playerConnection.getClass(),
                    PacketPlayOutOpenWindow);
            sendPacket.invoke(playerConnection, packet);

            // Set their active container to the container
            Field activeContainerField = NMSManager.get().getField(EntityHuman, "activeContainer");
            if (activeContainerField != null) {
                activeContainerField.set(p, container);

                // Set their active container window id to that counter stuff
                NMSManager.get().getField(NMSManager.get().getNMSClass("Container"), "windowId")
                        .set(activeContainerField.get(p), c);

                // Add the slot listener
                NMSManager.get().getMethod("addSlotListener", activeContainerField.get(p).getClass(), p.getClass())
                        .invoke(activeContainerField.get(p), p);
            }
        } catch (Exception e) {
            MatrixFramework.getInstance().debug(e);
            MatrixFramework.getInstance().debug("Failed to use AnvilGUI");
        }
    }

}
