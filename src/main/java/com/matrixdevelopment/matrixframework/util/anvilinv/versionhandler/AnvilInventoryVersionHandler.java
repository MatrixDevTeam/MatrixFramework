package com.matrixdevelopment.matrixframework.util.anvilinv.versionhandler;

import com.matrixdevelopment.matrixframework.util.anvilinv.AnvilInventory;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public interface AnvilInventoryVersionHandler {

    /**
     * Gets the inventory.
     *
     * @return the inventory
     */
    Inventory getInventory();

    /**
     * Load classes.
     */
    void loadClasses();

    /**
     * Open.
     *
     * @param player
     *            the player
     * @param items
     *            the items
     */
    void open(Player player, HashMap<AnvilInventory.AnvilSlot, ItemStack> items);

}
