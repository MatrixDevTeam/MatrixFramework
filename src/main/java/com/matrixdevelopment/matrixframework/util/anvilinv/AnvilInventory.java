package com.matrixdevelopment.matrixframework.util.anvilinv;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.util.anvilinv.versionhandler.AnvilInventoryReflectionHandler;
import com.matrixdevelopment.matrixframework.util.anvilinv.versionhandler.AnvilInventoryVersionHandler;
import com.matrixdevelopment.matrixframework.util.misc.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

public class AnvilInventory {

    public class AnvilClickEvent {

        private AnvilSlot slot;
        private String name;
        private boolean close, destroy = true;
        private Player player;

        /**
         * Instantiates a new anvil click event.
         *
         * @param slot
         *            the slot
         * @param name
         *            the name
         * @param player
         *            the player
         */
        public AnvilClickEvent(AnvilSlot slot, String name, Player player) {
            this.slot = slot;
            this.name = name;
            this.player = player;
        }

        /**
         * Gets the name.
         *
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Gets the player.
         *
         * @return the player
         */
        public Player getPlayer() {
            return player;
        }

        /**
         * Gets the slot.
         *
         * @return the slot
         */
        public AnvilSlot getSlot() {
            return slot;
        }

        /**
         * Gets the will close.
         *
         * @return the will close
         */
        public boolean getWillClose() {
            return close;
        }

        /**
         * Gets the will destroy.
         *
         * @return the will destroy
         */
        public boolean getWillDestroy() {
            return destroy;
        }

        /**
         * Sets the will close.
         *
         * @param close
         *            the new will close
         */
        public void setWillClose(boolean close) {
            this.close = close;
        }

        /**
         * Sets the will destroy.
         *
         * @param destroy
         *            the new will destroy
         */
        public void setWillDestroy(boolean destroy) {
            this.destroy = destroy;
        }

    }

    public interface AnvilClickEventHandler {

        /**
         * On anvil click
         *
         * @param e the Event
         */
        void onAnvilClick(AnvilClickEvent e);
    }

    public enum AnvilSlot {

        INPUT_LEFT(0),
        INPUT_RIGHT(1),
        OUTPUT(2);

        /**
         * By slot.
         *
         * @param slot
         *            the slot
         * @return the anvil slot
         */
        public static AnvilSlot bySlot(int slot) {
            for (AnvilSlot anvilSlot : values()) {
                if (anvilSlot.getSlot() == slot) {
                    return anvilSlot;
                }
            }

            return null;
        }

        /** The slot. */
        private int slot;

        /**
         * Instantiates a new anvil slot.
         *
         * @param slot
         *            the slot
         */
        AnvilSlot(int slot) {
            this.slot = slot;
        }

        /**
         * Gets the slot.
         *
         * @return the slot
         */
        public int getSlot() {
            return slot;
        }
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public static String getVersion() {
        Server server = Bukkit.getServer();
        final String packageName = server.getClass().getPackage().getName();

        return packageName.substring(packageName.lastIndexOf('.') + 1);
    }

    private AnvilInventoryVersionHandler versionHandler;
    private Player player;
    private AnvilClickEventHandler handler;
    private HashMap<AnvilSlot, ItemStack> items = new HashMap();
    private Listener listener;

    /**
     * Instantiates a new a inventory.
     *
     * @param player
     *            the player
     * @param anvilClickEventHandler
     *            the anvil click event handler
     */
    public AnvilInventory(final Player player, final AnvilClickEventHandler anvilClickEventHandler) {

        versionHandler= new AnvilInventoryReflectionHandler(player, anvilClickEventHandler);

        this.player = player;
        handler = anvilClickEventHandler;
        PlayerUtils.getInstance().setPlayerMeta(player, "AnvilInventory", anvilClickEventHandler);

        listener = new Listener() {
            @EventHandler
            public void onInventoryClick(InventoryClickEvent event) {
                if (event.getWhoClicked() instanceof Player) {

                    if (event.getInventory().equals(versionHandler.getInventory())) {
                        event.setCancelled(true);

                        ItemStack item = event.getCurrentItem();
                        int slot = event.getRawSlot();
                        String name = "";

                        if (item != null) {
                            if (item.hasItemMeta()) {
                                ItemMeta meta = item.getItemMeta();

                                if (meta.hasDisplayName()) {
                                    name = meta.getDisplayName();
                                }
                            }
                        }

                        AnvilClickEvent clickEvent = new AnvilClickEvent(AnvilSlot.bySlot(slot), name,
                                (Player) event.getWhoClicked());

                        if (clickEvent.getSlot() == AnvilSlot.OUTPUT) {
                            event.getWhoClicked().closeInventory();
                            if (handler == null) {
                                handler = (AnvilClickEventHandler) PlayerUtils.getInstance().getPlayerMeta(player,
                                        "AnvilInventory");
                                // MatrixFramework.getInstance().debug("Anvil
                                // handler was null, fixing...");
                            }

                            Bukkit.getScheduler().runTaskAsynchronously(MatrixFramework.getInstance().getPlugin(),
                                    new Runnable() {

                                        @Override
                                        public void run() {
                                            if (handler == null) {
                                                handler = (AnvilClickEventHandler) PlayerUtils.getInstance().getPlayerMeta(player, "AnvilInventory");
                                            }
                                            handler.onAnvilClick(clickEvent);
                                        }
                                    });

                            destroy();
                        }

                    }
                }
            }

            @EventHandler
            public void onInventoryClose(InventoryCloseEvent event) {
                if (event.getPlayer() instanceof Player) {
                    Inventory inv = event.getInventory();
                    player.setLevel(player.getLevel() - 1);
                    if (inv.equals(versionHandler.getInventory())) {
                        inv.clear();
                        destroy();
                    }
                }
            }

            @EventHandler
            public void onPlayerQuit(PlayerQuitEvent event) {
                if (event.getPlayer().equals(getPlayer())) {
                    player.setLevel(player.getLevel() - 1);
                    destroy();
                }
            }
        };

        Bukkit.getPluginManager().registerEvents(listener, MatrixFramework.getInstance().getPlugin());
    }

    /**
     * Destroy.
     */
    public void destroy() {
        player = null;
        handler = null;
        items = null;

        HandlerList.unregisterAll(listener);

        listener = null;
    }

    /**
     * Gets the player.
     *
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Open.
     */
    public void open() {
        versionHandler.open(getPlayer(), items);
    }

    /**
     * Sets the slot.
     *
     * @param slot
     *            the slot
     * @param item
     *            the item
     */
    public void setSlot(AnvilSlot slot, ItemStack item) {
        items.put(slot, item);
    }

}
