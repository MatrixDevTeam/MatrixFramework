package com.matrixdevelopment.matrixframework.util.misc;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.TabCompleter;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class PluginUtils {

    static PluginUtils INSTANCE = new PluginUtils();

    public static PluginUtils getInstance() {
        return INSTANCE;
    }

    MatrixFramework plugin = MatrixFramework.getInstance();

    private PluginUtils() {}

    public void registerCommands(JavaPlugin plugin, String commandText, CommandExecutor executor, TabCompleter tab) {
        plugin.getCommand(commandText).setExecutor(executor);
        if (tab != null) {
            plugin.getCommand(commandText).setTabCompleter(tab);
        }
    }

    public void registerEvents(Listener listener, JavaPlugin plugin) {
        Bukkit.getPluginManager().registerEvents(listener, plugin);
    }

}
