package com.matrixdevelopment.matrixframework.util.logger;

import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Logger {

    Plugin plugin;
    File location;

    /**
     * Instantiates a new logger.
     *
     * @param plugin the Plugin
     * @param location the file Location
     */
    public Logger(Plugin plugin, File location) {
        this.plugin = plugin;
        this.location = location;
    }

    public void logToFile(String msg) {
        try {
            if (!location.getParentFile().exists()) {
                location.getParentFile().mkdirs();
            }

            File saveTo = location;

            if (!saveTo.exists()) {
                saveTo.createNewFile();
            }

            FileWriter fw = new FileWriter(saveTo, true);
            PrintWriter pw = new PrintWriter(fw);

            pw.println(msg);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
