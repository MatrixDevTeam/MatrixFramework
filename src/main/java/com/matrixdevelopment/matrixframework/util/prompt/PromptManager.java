package com.matrixdevelopment.matrixframework.util.prompt;

import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.ConversationFactory;

public class PromptManager {

    ConversationFactory conversationFactory;

    String promptText;

    /**
     * Instantiates a new prompt manager.
     *
     * @param promptText
     *            the prompt text
     * @param convoFactory
     *            the convo factory
     */
    public PromptManager(String promptText, ConversationFactory convoFactory) {
        this.promptText = promptText;
        conversationFactory = convoFactory;
    }

    /**
     * String prompt.
     *
     * @param conversable
     *            the conversable
     * @param prompt
     *            the prompt
     */
    public void stringPrompt(Conversable conversable, PromptReturnString prompt) {
        prompt.promptText = promptText;
        conversationFactory.withFirstPrompt(prompt).buildConversation(conversable).begin();
    }

}
