package com.matrixdevelopment.matrixframework.util.updater;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import org.bukkit.plugin.java.JavaPlugin;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class Updater {

    /**
     * The Enum UpdateResult.
     */
    public enum UpdateResult {

        /** The bad resourceid. */
        BAD_RESOURCEID,

        /** The disabled. */
        DISABLED,

        /** The fail noversion. */
        FAIL_NOVERSION,

        /** The fail spigot. */
        FAIL_SPIGOT,

        /** The no update. */
        NO_UPDATE,

        /** The update available. */
        UPDATE_AVAILABLE
    }

    /** The old version. */
    private String oldVersion;

    /** The plugin. */
    private JavaPlugin plugin;

    /** The resource id. */
    private String RESOURCE_ID = "";

    /** The result. */
    private Updater.UpdateResult result = Updater.UpdateResult.DISABLED;

    /** The version. */
    private String version;

    /**
     * Instantiates a new updater.
     *
     * @param plugin
     *            the plugin
     * @param resourceId
     *            the resource id
     * @param disabled
     *            the disabled
     */
    public Updater(JavaPlugin plugin, Integer resourceId, boolean disabled) {
        RESOURCE_ID = resourceId + "";
        this.plugin = plugin;
        oldVersion = this.plugin.getDescription().getVersion();

        if (disabled) {
            result = UpdateResult.DISABLED;
            return;
        }

        // WRITE_STRING = "key=" + API_KEY + "&resource=" + RESOURCE_ID;
        run();
    }

    /**
     * Gets the result.
     *
     * @return the result
     */
    public UpdateResult getResult() {
        return result;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    private void run() {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) new URL(
                    "https://api.spigotmc.org/legacy/update.php?resource=" + RESOURCE_ID).openConnection();
            int timed_out = 1250; // check if API is available, set your time as you want
            connection.setConnectTimeout(timed_out);
            connection.setReadTimeout(timed_out);
            this.version = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
            connection.disconnect();
            versionCheck();
            return;
        } catch (Exception e) {
            result = UpdateResult.FAIL_SPIGOT;
            MatrixFramework.getInstance().debug(e);
        }
        result = UpdateResult.FAIL_SPIGOT;
    }

    /**
     * Should update.
     *
     * @param localVersion
     *            the local version
     * @param remoteVersion
     *            the remote version
     * @return true, if successful
     */
    public boolean shouldUpdate(String localVersion, String remoteVersion) {
        return !localVersion.equalsIgnoreCase(remoteVersion);
    }

    /**
     * Version check.
     */
    private void versionCheck() {
        if (shouldUpdate(oldVersion, version)) {
            result = UpdateResult.UPDATE_AVAILABLE;
        } else {
            result = UpdateResult.NO_UPDATE;
        }
    }

}
