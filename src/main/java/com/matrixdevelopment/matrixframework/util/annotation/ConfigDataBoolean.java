package com.matrixdevelopment.matrixframework.util.annotation;

import java.lang.annotation.*;

@Inherited
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigDataBoolean {

    boolean defaultValue() default false;

    String path();

}
