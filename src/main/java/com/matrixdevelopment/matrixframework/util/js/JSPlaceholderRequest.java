package com.matrixdevelopment.matrixframework.util.js;

import org.bukkit.OfflinePlayer;

public abstract class JSPlaceholderRequest {

    private String str;

    public JSPlaceholderRequest(String str) {
        this.str = str;
    }

    public abstract Object getObject(OfflinePlayer player);

    public String getSTR() {
        return str;
    }

    public void setSTR(String str) {
        this.str = str;
    }

}
