package com.matrixdevelopment.matrixframework.util.js;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.usermanagement.User;
import com.matrixdevelopment.matrixframework.usermanagement.UserManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.HashMap;
import java.util.Map;

public class JSEngine {

    private HashMap<String, Object> engineAPI;

    public JSEngine() {
        engineAPI = new HashMap();
    }

    public JSEngine addPlayer(CommandSender sender) {
        addToEngine("CommandSender", sender);

        if (sender instanceof Player) {
            Player p = (Player) sender;

            addToEngine("Player", p);
            addToEngine("PlayerName", p.getName());
            addToEngine("PlayerUUID", p.getUniqueId().toString());
            addToEngine("MatrixUser", UserManager.getInstance().getUser(p));

            for (JSPlaceholderRequest request : MatrixFramework.getInstance().getJavascriptEngineRequests()) {
                addToEngine(request.getSTR(), request.getObject(p));
            }
        } else {
            addToEngine("Player", sender);
        }
        return this;
    }

    public JSEngine addPlayer(OfflinePlayer player) {
        addToEngine("Player", player);
        addToEngine("PlayerName", player.getName());
        addToEngine("PlayerUUID", player.getUniqueId().toString());
        addToEngine("MatrixUser", UserManager.getInstance().getUser(player));
        addToEngine("CommandSender", player);

        for (JSPlaceholderRequest request : MatrixFramework.getInstance().getJavascriptEngineRequests()) {
            addToEngine(request.getSTR(), request.getObject(player));
        }

        if (player.isOnline()) {
            return addPlayer(player.getPlayer());
        }
        return this;
    }

    public JSEngine addPlayer(Player player) {
        addToEngine("Player", player);
        addToEngine("PlayerName", player.getName());
        addToEngine("PlayerUUID", player.getUniqueId().toString());
        addToEngine("MatrixUser", UserManager.getInstance().getUser(player));
        addToEngine("CommandSender", player);

        for (JSPlaceholderRequest request : MatrixFramework.getInstance().getJavascriptEngineRequests()) {
            addToEngine(request.getSTR(), request.getObject(player));
        }
        return this;
    }

    public JSEngine addPlayer(User user) {
        addToEngine("PlayerName", user.getPlayerName());
        addToEngine("PlayerUUID", user.getUUID());
        addToEngine("MatrixUser", user);

        for (JSPlaceholderRequest request : MatrixFramework.getInstance().getJavascriptEngineRequests()) {
            addToEngine(request.getSTR(), request.getObject(user.getOfflinePlayer()));
        }

        if (user.isOnline()) {
            return addPlayer(user.getPlayer());
        }
        return this;
    }

    public JSEngine addToEngine(HashMap<String, Object> engineAPI) {
        if (engineAPI != null && !engineAPI.isEmpty()) {
            this.engineAPI.putAll(engineAPI);
        }
        return this;
    }

    public JSEngine addToEngine(String text, Object ob) {
        engineAPI.put(text, ob);
        return this;
    }

    public void execute(String expression) {
        getResult(expression);
    }

    public boolean getBooleanValue(String expression) {
        Object result = getResult(expression);
        if (result != null) {
            try {
                return ((boolean) result);
            } catch (Exception e) {
                MatrixFramework.getInstance().debug(e);
            }
        }
        return false;
    }

    public Object getResult(String expression) {
        if (!expression.equals("")) {
            ScriptEngine engine = new ScriptEngineManager().getEngineByName("javascript");
            engine.put("Bukkit", Bukkit.getServer());
            engine.put("MatrixFramework", MatrixFramework.getInstance());
            engine.put("Console", Bukkit.getConsoleSender());
            engine.put("UserManager", UserManager.getInstance());

            engineAPI.putAll(MatrixFramework.getInstance().getJavascriptEngine());

            for (Map.Entry<String, Object> entry : engineAPI.entrySet()) {
                engine.put(entry.getKey(), entry.getValue());
            }

            try {
                return engine.eval(expression);
            } catch (ScriptException e) {
                MatrixFramework.getInstance().getPlugin().getLogger().warning("Error occurred while evaluating javascript, enable debug to see stacktrace: " + e.toString());
                MatrixFramework.getInstance().debug(e);
            }
        }
        return null;
    }

    public String getStringValue(String expression) {
        Object result = getResult(expression);
        if (result != null) {
            try {
                return result.toString();
            } catch (Exception e) {
                MatrixFramework.getInstance().debug(e);
            }
        }
        return "";
    }
}
