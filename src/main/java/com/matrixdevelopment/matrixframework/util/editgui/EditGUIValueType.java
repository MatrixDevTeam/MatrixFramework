package com.matrixdevelopment.matrixframework.util.editgui;

public enum EditGUIValueType {
    NUMBER, STRING, BOOLEAN, LIST
}
