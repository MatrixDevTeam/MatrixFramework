package com.matrixdevelopment.matrixframework.util.editgui;

import com.matrixdevelopment.matrixframework.util.inv.BInventory;
import com.matrixdevelopment.matrixframework.util.inv.BInventoryButton;
import com.matrixdevelopment.matrixframework.util.misc.ArrayUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class EditGUI extends BInventory {

    public EditGUI(String name) {
        super(name);
    }

    public void sort() {
        Map<Integer, BInventoryButton> map = getButtons();
        setButtons(new HashMap());
        LinkedHashMap<String, EditGUIButton> buttons = new LinkedHashMap<String, EditGUIButton>();
        for (BInventoryButton button : map.values()) {
            if (button instanceof EditGUIButton) {
                EditGUIButton b = (EditGUIButton) button;
                buttons.put(b.getKey(), b);
            } else {
                addButton(button);
            }
        }

        ArrayList<String> keys = ArrayUtils.getInstance().convert(buttons.keySet());
        keys = ArrayUtils.getInstance().sort(keys);

        for (String key : keys) {
            addButton(buttons.get(key));
        }
    }

}
