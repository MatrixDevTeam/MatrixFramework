package com.matrixdevelopment.matrixframework.util.editgui;

import com.matrixdevelopment.matrixframework.util.inv.BInventory;
import com.matrixdevelopment.matrixframework.util.inv.BInventoryButton;
import com.matrixdevelopment.matrixframework.util.item.ItemBuilder;
import com.matrixdevelopment.matrixframework.util.misc.ArrayUtils;
import com.matrixdevelopment.matrixframework.util.value.ValueRequestBuilder;
import com.matrixdevelopment.matrixframework.util.value.listeners.BooleanListener;
import com.matrixdevelopment.matrixframework.util.value.listeners.Listener;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public abstract class EditGUIButton extends BInventoryButton {

    private String key;

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key
     *            the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    private Object value;

    public EditGUIButton(ItemBuilder item, String key, Object value, EditGUIValueType type) {
        super(item);
        setValueType(type);
        this.key = key;
        this.value = value;
        if (!type.equals(EditGUIValueType.LIST)) {
            getBuilder().setName("&cSet " + type.toString() + " for " + key);
            getBuilder().addLoreLine("&cCurrent value: " + value);
        } else {
            getBuilder().setName("&cEdit list for " + key);
            getBuilder().addLoreLine(ArrayUtils.getInstance().makeStringList((ArrayList<String>) value));
        }
    }

    private EditGUIValueType type;

    public EditGUIButton setValueType(EditGUIValueType type) {
        this.type = type;
        return this;
    }

    private ArrayList<String> options = new ArrayList<String>();

    public EditGUIButton setOptions(String... str) {
        for (String s : str) {
            options.add(s);
        }
        return this;
    }

    @Override
    public void onClick(BInventory.ClickEvent clickEvent) {
        if (type.equals(EditGUIValueType.BOOLEAN)) {
            new ValueRequestBuilder(new BooleanListener() {

                @Override
                public void onInput(Player player, boolean value) {
                    setValue(player, value);
                    sendMessage(player, "&cSetting " + getKey() + " to " + value);
                }
            }).currentValue(value.toString()).request(clickEvent.getPlayer());
        } else if (type.equals(EditGUIValueType.NUMBER)) {
            new ValueRequestBuilder(new Listener<Number>() {
                @Override
                public void onInput(Player player, Number number) {
                    setValue(player, number.doubleValue());
                    sendMessage(player, "&cSetting " + getKey() + " to " + value);
                }
            }, new Number[] { 0, 10, 25, 50, 100, 500, 1000, (Number) value }).currentValue(value.toString())
                    .request(clickEvent.getPlayer());
        } else if (type.equals(EditGUIValueType.STRING)) {
            new ValueRequestBuilder(new Listener<String>() {
                @Override
                public void onInput(Player player, String value) {
                    setValue(player, value);
                    sendMessage(player, "&cSetting " + getKey() + " to " + value);
                }
            }, ArrayUtils.getInstance().convert(options)).currentValue(value.toString()).allowCustomOption(true)
                    .request(clickEvent.getPlayer());
        } else if (type.equals(EditGUIValueType.LIST)) {
            BInventory inv = new BInventory("Edit list: " + key);
            inv.setMeta(clickEvent.getPlayer(), "Value", value);
            inv.addButton(new BInventoryButton(new ItemBuilder(Material.EMERALD_BLOCK).setName("&cAdd value")) {

                @Override
                public void onClick(BInventory.ClickEvent clickEvent) {
                    new ValueRequestBuilder(new Listener<String>() {
                        @Override
                        public void onInput(Player player, String add) {

                            ArrayList<String> list = (ArrayList<String>) getMeta(player, "Value");
                            if (list == null) {
                                list = new ArrayList<String>();
                            }
                            list.add(add);
                            setValue(player, value);
                            sendMessage(player, "&cAdded " + add + " to " + getKey());
                        }
                    }, new String[] {}).request(clickEvent.getPlayer());
                }
            });
            inv.addButton(new BInventoryButton(new ItemBuilder(Material.BARRIER).setName("&cRemove value")) {

                @Override
                public void onClick(BInventory.ClickEvent clickEvent) {
                    new ValueRequestBuilder(new Listener<String>() {
                        @Override
                        public void onInput(Player player, String add) {
                            ArrayList<String> list = (ArrayList<String>) getMeta(player, "Value");
                            list.remove(add);
                            setValue(player, value);
                            sendMessage(player, "&cRemoved " + add + " from " + getKey());
                        }
                    }, ArrayUtils.getInstance().convert((ArrayList<String>) getData("Value")))
                            .request(clickEvent.getPlayer());
                }
            });
            inv.openInventory(clickEvent.getPlayer());
        }
    }

    /**
     * @return the type
     */
    public EditGUIValueType getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(EditGUIValueType type) {
        this.type = type;
    }

    public abstract void setValue(Player player, Object value);

}
