package com.matrixdevelopment.matrixframework.util.value.listeners;

import org.bukkit.entity.Player;

public abstract class StringListener {

    /**
     * On Input
     *
     * @param player the Player
     * @param value the Value
     */
    public abstract void onInput(Player player, String value);

}
