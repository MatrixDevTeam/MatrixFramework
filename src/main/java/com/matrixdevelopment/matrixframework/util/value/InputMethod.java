package com.matrixdevelopment.matrixframework.util.value;

import com.matrixdevelopment.matrixframework.MatrixFramework;

public enum InputMethod {

    ANVIL,
    CHAT,
    BOOK,
    INVENTORY,
    SIGN;

    /**
     * Get the method
     *
     * @param method the Method
     * @return the Requested method
     */
    public static InputMethod getMethod(String method) {
        for (InputMethod input : values()) {
            if (method.equalsIgnoreCase(input.toString())) {
                return input;
            }
        }

        try {
            return valueOf(MatrixFramework.getInstance().getDefaultRequestMethod());
        } catch (Exception e) {
            return CHAT;
        }
    }

}
