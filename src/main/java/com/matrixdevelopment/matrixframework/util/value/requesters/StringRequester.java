package com.matrixdevelopment.matrixframework.util.value.requesters;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.usermanagement.User;
import com.matrixdevelopment.matrixframework.usermanagement.UserManager;
import com.matrixdevelopment.matrixframework.util.anvilinv.AnvilInventory;
import com.matrixdevelopment.matrixframework.util.book.BookManager;
import com.matrixdevelopment.matrixframework.util.book.BookSign;
import com.matrixdevelopment.matrixframework.util.inv.BInventory;
import com.matrixdevelopment.matrixframework.util.inv.BInventoryButton;
import com.matrixdevelopment.matrixframework.util.item.ItemBuilder;
import com.matrixdevelopment.matrixframework.util.misc.ArrayUtils;
import com.matrixdevelopment.matrixframework.util.misc.PlayerUtils;
import com.matrixdevelopment.matrixframework.util.prompt.PromptManager;
import com.matrixdevelopment.matrixframework.util.prompt.PromptReturnString;
import com.matrixdevelopment.matrixframework.util.sign.SignMenu;
import com.matrixdevelopment.matrixframework.util.value.InputMethod;
import com.matrixdevelopment.matrixframework.util.value.ValueRequest;
import com.matrixdevelopment.matrixframework.util.value.listeners.StringListener;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class StringRequester {

    /**
     * Instantiates a new string requester.
     */
    public StringRequester() {

    }

    /**
     * Request.
     *
     * @param player
     *            the player
     * @param method
     *            the method
     * @param currentValue
     *            the current value
     * @param promptText
     *            the prompt text
     * @param options
     *            the options
     * @param allowCustomOption
     *            the allow custom option
     * @param listener
     *            the listener
     */
    public void request(Player player, InputMethod method, String currentValue,
                        LinkedHashMap<String, ItemStack> options, String promptText, boolean allowCustomOption,
                        StringListener listener) {
        if ((options == null || options.size() == 0) && method.equals(InputMethod.INVENTORY) && allowCustomOption) {
            method = InputMethod.ANVIL;
        }
        if ((options != null && options.size() != 0) && method.equals(InputMethod.ANVIL)) {
            method = InputMethod.INVENTORY;
        }
        if (MatrixFramework.getInstance().getDisabledRequestMethods().contains(method.toString())) {
            player.sendMessage("Disabled method: " + method.toString());
        }
        if (method.equals(InputMethod.INVENTORY)) {
            if (options == null) {
                player.sendMessage("There are no choices to choice from to use this method");
                return;
            }

            BInventory inv = new BInventory("Click one of the following:");
            for (Map.Entry<String, ItemStack> entry : options.entrySet()) {
                inv.addButton(inv.getNextSlot(),
                        new BInventoryButton(entry.getKey(), new String[] {}, entry.getValue()) {

                            @Override
                            public void onClick(BInventory.ClickEvent clickEvent) {
                                listener.onInput(clickEvent.getPlayer(),
                                        clickEvent.getClickedItem().getItemMeta().getDisplayName());

                            }
                        });
            }

            if (allowCustomOption) {
                inv.addButton(inv.getNextSlot(), new BInventoryButton("&cClick to enter custom value", new String[] {},
                        new ItemStack(Material.ANVIL)) {

                    @Override
                    public void onClick(BInventory.ClickEvent clickEvent) {
                        new ValueRequest().requestString(clickEvent.getPlayer(), listener);
                    }
                });
            }

            inv.openInventory(player);

        } else if (method.equals(InputMethod.ANVIL)) {

            AnvilInventory inv = new AnvilInventory(player, new AnvilInventory.AnvilClickEventHandler() {

                @Override
                public void onAnvilClick(AnvilInventory.AnvilClickEvent event) {
                    Player player = event.getPlayer();
                    if (event.getSlot() == AnvilInventory.AnvilSlot.OUTPUT) {

                        event.setWillClose(true);
                        event.setWillDestroy(true);

                        listener.onInput(player, event.getName());

                    } else {
                        event.setWillClose(false);
                        event.setWillDestroy(false);
                    }
                }
            });

            ItemBuilder builder = new ItemBuilder(Material.NAME_TAG);
            builder.setName(currentValue);

            ArrayList<String> lore = new ArrayList<String>();
            lore.add("&cRename item and take out to set value");
            lore.add("&cDoes not cost exp");
            builder.setLore(lore);

            inv.setSlot(AnvilInventory.AnvilSlot.INPUT_LEFT, builder.toItemStack(player));

            inv.open();

        } else if (method.equals(InputMethod.CHAT)) {

            if (options != null && options.size() != 0) {
                User user = UserManager.getInstance().getUser(player);
                user.sendMessage("&cClick one of the following options below:");
                PlayerUtils.getInstance().setPlayerMeta(player, "ValueRequestString", listener);
                for (String option : options.keySet()) {
                    TextComponent comp = new TextComponent(option);
                    comp.setClickEvent(new net.md_5.bungee.api.chat.ClickEvent(ClickEvent.Action.RUN_COMMAND,
                            "/" + MatrixFramework.getInstance().getPlugin().getName() + "valuerequestinput String "
                                    + option));
                    user.sendJson(comp);
                }
                if (allowCustomOption) {
                    String option = "CustomValue";
                    TextComponent comp = new TextComponent(option);
                    comp.setClickEvent(new net.md_5.bungee.api.chat.ClickEvent(ClickEvent.Action.RUN_COMMAND,
                            "/" + MatrixFramework.getInstance().getPlugin().getName() + "valuerequestinput String "
                                    + option));
                    user.sendJson(comp);
                }
            } else {
                ConversationFactory convoFactory = new ConversationFactory(MatrixFramework.getInstance().getPlugin())
                        .withModality(true).withEscapeSequence("cancel").withTimeout(60);
                PromptManager prompt = new PromptManager(promptText + " Current value: " + currentValue, convoFactory);
                prompt.stringPrompt(player, new PromptReturnString() {

                    @Override
                    public void onInput(ConversationContext context, Conversable conversable, String input) {
                        Bukkit.getScheduler().runTaskAsynchronously(MatrixFramework.getInstance().getPlugin(), new Runnable() {

                            @Override
                            public void run() {
                                listener.onInput((Player) conversable, input);
                            }
                        });

                    }
                });
            }
        } else if (method.equals(InputMethod.BOOK)) {

            new BookManager(player, currentValue, new BookSign() {

                @Override
                public void onBookSign(Player player, String input) {
                    listener.onInput(player, input);

                }
            });
        } else if (method.equals(InputMethod.SIGN)) {
            MatrixFramework.getInstance().getSignMenu().open(player.getUniqueId(), new String[] { "", "", "", "" },
                    new SignMenu.InputReceiver() {

                        @Override
                        public void receive(Player player, String[] text) {
                            String str = "";
                            for (String t : text) {
                                str += t;
                            }
                            listener.onInput(player, str);

                        }
                    });
        } else {
            player.sendMessage("Invalid method/disabled method, change your request method");
        }
    }

    public void request(Player player, InputMethod method, String currentValue, String promptText, String[] options,
                        boolean allowCustomOption, StringListener listener) {
        LinkedHashMap<String, ItemStack> items = new LinkedHashMap<String, ItemStack>();
        if (options != null) {
            for (String option : options) {
                items.put(option, new ItemStack(Material.STONE, 1));
            }
        }
        items = ArrayUtils.getInstance().sortByValuesStrItem(items);
        request(player, method, currentValue, items, promptText, allowCustomOption, listener);
    }

}
