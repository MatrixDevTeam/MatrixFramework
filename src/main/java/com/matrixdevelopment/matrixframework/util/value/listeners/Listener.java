package com.matrixdevelopment.matrixframework.util.value.listeners;

import org.bukkit.entity.Player;

public abstract class Listener<T> {

    public abstract void onInput(Player player, T value);

}
