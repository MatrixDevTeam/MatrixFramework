package com.matrixdevelopment.matrixframework.util.value.requesters;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.usermanagement.User;
import com.matrixdevelopment.matrixframework.usermanagement.UserManager;
import com.matrixdevelopment.matrixframework.util.anvilinv.AnvilInventory;
import com.matrixdevelopment.matrixframework.util.book.BookManager;
import com.matrixdevelopment.matrixframework.util.book.BookSign;
import com.matrixdevelopment.matrixframework.util.inv.BInventory;
import com.matrixdevelopment.matrixframework.util.inv.BInventoryButton;
import com.matrixdevelopment.matrixframework.util.item.ItemBuilder;
import com.matrixdevelopment.matrixframework.util.misc.PlayerUtils;
import com.matrixdevelopment.matrixframework.util.value.InputMethod;
import com.matrixdevelopment.matrixframework.util.value.listeners.BooleanListener;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class BooleanRequester {

    /**
     * Instantiates a new boolean requester.
     */
    public BooleanRequester() {

    }

    /**
     * Request.
     *
     * @param player
     *            the player
     * @param method
     *            the method
     * @param currentValue
     *            the current value
     * @param promptText
     *            the prompt text
     * @param listener
     *            the listener
     */
    public void request(Player player, InputMethod method, String currentValue, String promptText,
                        BooleanListener listener) {
        if (MatrixFramework.getInstance().getDisabledRequestMethods().contains(method.toString())) {
            player.sendMessage("Disabled method: " + method.toString());
        }
        if (method.equals(InputMethod.SIGN)) {
            method = InputMethod.INVENTORY;
        }
        if (method.equals(InputMethod.INVENTORY)) {

            BInventory inv = new BInventory("Click one of the following:");

            inv.addButton(inv.getNextSlot(),
                    new BInventoryButton("True", new String[] {}, new ItemStack(Material.REDSTONE_BLOCK)) {

                        @Override
                        public void onClick(BInventory.ClickEvent clickEvent) {
                            listener.onInput(clickEvent.getPlayer(),
                                    Boolean.valueOf(clickEvent.getClickedItem().getItemMeta().getDisplayName()));

                        }
                    });
            inv.addButton(inv.getNextSlot(),
                    new BInventoryButton("False", new String[] {}, new ItemStack(Material.IRON_BLOCK)) {

                        @Override
                        public void onClick(BInventory.ClickEvent clickEvent) {
                            listener.onInput(clickEvent.getPlayer(),
                                    Boolean.valueOf(clickEvent.getClickedItem().getItemMeta().getDisplayName()));

                        }
                    });

            inv.openInventory(player);

        } else if (method.equals(InputMethod.ANVIL)) {

            AnvilInventory inv = new AnvilInventory(player, new AnvilInventory.AnvilClickEventHandler() {

                @Override
                public void onAnvilClick(AnvilInventory.AnvilClickEvent event) {
                    Player player = event.getPlayer();
                    if (event.getSlot() == AnvilInventory.AnvilSlot.OUTPUT) {

                        event.setWillClose(true);
                        event.setWillDestroy(true);

                        listener.onInput(player, Boolean.valueOf(event.getName()));

                    } else {
                        event.setWillClose(false);
                        event.setWillDestroy(false);
                    }
                }
            });

            ItemBuilder builder = new ItemBuilder(Material.NAME_TAG);
            builder.setName(currentValue);

            ArrayList<String> lore = new ArrayList<String>();
            lore.add("&cRename item and take out to set value");
            lore.add("&cDoes not cost exp");
            builder.setLore(lore);

            inv.setSlot(AnvilInventory.AnvilSlot.INPUT_LEFT, builder.toItemStack(player));

            inv.open();

        } else if (method.equals(InputMethod.CHAT)) {

            User user = UserManager.getInstance().getUser(player);
            user.sendMessage("&cClick one of the following options below:");
            String option = "True";
            TextComponent comp = new TextComponent(option);
            PlayerUtils.getInstance().setPlayerMeta(player, "ValueRequestBoolean", listener);
            comp.setClickEvent(new net.md_5.bungee.api.chat.ClickEvent(ClickEvent.Action.RUN_COMMAND, "/"
                    + MatrixFramework.getInstance().getPlugin().getName() + "valuerequestinput Boolean " + option));
            user.sendJson(comp);
            option = "False";
            comp = new TextComponent(option);
            comp.setClickEvent(new net.md_5.bungee.api.chat.ClickEvent(ClickEvent.Action.RUN_COMMAND, "/"
                    + MatrixFramework.getInstance().getPlugin().getName() + "valuerequestinput Boolean " + option));
            user.sendJson(comp);
        } else if (method.equals(InputMethod.BOOK)) {

            new BookManager(player, currentValue, new BookSign() {

                @Override
                public void onBookSign(Player player, String input) {
                    listener.onInput(player, Boolean.valueOf(input));

                }
            });
        } else {
            player.sendMessage("Invalid method/disabled method, change your request method");
        }
    }

}
