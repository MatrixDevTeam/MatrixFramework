package com.matrixdevelopment.matrixframework.util.value;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.usermanagement.UserManager;
import com.matrixdevelopment.matrixframework.util.value.listeners.BooleanListener;
import com.matrixdevelopment.matrixframework.util.value.listeners.NumberListener;
import com.matrixdevelopment.matrixframework.util.value.listeners.StringListener;
import com.matrixdevelopment.matrixframework.util.value.requesters.BooleanRequester;
import com.matrixdevelopment.matrixframework.util.value.requesters.NumberRequester;
import com.matrixdevelopment.matrixframework.util.value.requesters.StringRequester;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.LinkedHashMap;

public class ValueRequest {

    /** The plugin. */
    MatrixFramework plugin = MatrixFramework.getInstance();

    /** The method. */
    private InputMethod method = null;

    /**
     * Instantiates a new value request.
     */
    public ValueRequest() {
    }

    /**
     * Instantiates a new value request.
     *
     * @param method
     *            the method
     */
    public ValueRequest(InputMethod method) {
        this.method = method;
    }

    /**
     * Request boolean.
     *
     * @param player
     *            the player
     * @param listener
     *            the listener
     */
    public void requestBoolean(Player player, BooleanListener listener) {
        InputMethod input = method;
        if (input == null) {
            input = UserManager.getInstance().getUser(player).getUserInputMethod();
        }
        new BooleanRequester().request(player, input, "", "Type cancel to cancel", listener);
    }

    /**
     * Request boolean.
     *
     * @param player
     *            the player
     * @param currentValue
     *            the current value
     * @param listener
     *            the listener
     */
    public void requestBoolean(Player player, String currentValue, BooleanListener listener) {
        InputMethod input = method;
        if (input == null) {
            input = UserManager.getInstance().getUser(player).getUserInputMethod();
        }
        new BooleanRequester().request(player, input, currentValue, "Type cancel to cancel", listener);
    }

    public void requestNumber(Player player, LinkedHashMap<Number, ItemStack> options, String currentValue,
                              boolean allowCustomOption, NumberListener listener) {
        InputMethod input = method;
        if (input == null) {
            input = UserManager.getInstance().getUser(player).getUserInputMethod();
        }
        new NumberRequester().request(player, input, currentValue, options, "Type cancel to cancel", allowCustomOption,
                listener);
    }

    /**
     * Request number.
     *
     * @param player
     *            the player
     * @param listener
     *            the listener
     */
    public void requestNumber(Player player, NumberListener listener) {
        InputMethod input = method;
        if (input == null) {
            input = UserManager.getInstance().getUser(player).getUserInputMethod();
        }
        new NumberRequester().request(player, input, "", "Type cancel to cancel", null, true, listener);
    }

    /**
     * Request number.
     *
     * @param player
     *            the player
     * @param currentValue
     *            the current value
     * @param options
     *            the options
     * @param allowCustomOption
     *            the allow custom option
     * @param listener
     *            the listener
     */
    public void requestNumber(Player player, String currentValue, Number[] options, boolean allowCustomOption,
                              NumberListener listener) {
        InputMethod input = method;
        if (input == null) {
            input = UserManager.getInstance().getUser(player).getUserInputMethod();
        }
        new NumberRequester().request(player, input, currentValue, "Type cancel to cancel", options, allowCustomOption,
                listener);
    }

    /**
     * Request number.
     *
     * @param player
     *            the player
     * @param currentValue
     *            the current value
     * @param options
     *            the options
     * @param listener
     *            the listener
     */
    public void requestNumber(Player player, String currentValue, Number[] options, NumberListener listener) {
        InputMethod input = method;
        if (input == null) {
            input = UserManager.getInstance().getUser(player).getUserInputMethod();
        }
        new NumberRequester().request(player, input, currentValue, "Type cancel to cancel", options, true, listener);
    }

    public void requestString(Player player, LinkedHashMap<String, ItemStack> options, String currentValue,
                              boolean allowCustomOption, StringListener listener) {
        InputMethod input = method;
        if (input == null) {
            input = UserManager.getInstance().getUser(player).getUserInputMethod();
        }
        new StringRequester().request(player, input, currentValue, options, "Type cancel to cancel", allowCustomOption,
                listener);
    }

    /**
     * Request string.
     *
     * @param player
     *            the player
     * @param currentValue
     *            the current value
     * @param options
     *            the options
     * @param allowCustomOption
     *            the allow custom option
     * @param listener
     *            the listener
     */
    public void requestString(Player player, String currentValue, String[] options, boolean allowCustomOption,
                              StringListener listener) {
        InputMethod input = method;
        if (input == null) {
            input = UserManager.getInstance().getUser(player).getUserInputMethod();
        }
        new StringRequester().request(player, input, currentValue, "Type cancel to cancel", options, allowCustomOption,
                listener);
    }

    /**
     * Request string.
     *
     * @param player
     *            the player
     * @param currentValue
     *            the current value
     * @param options
     *            the options
     * @param listener
     *            the listener
     */
    public void requestString(Player player, String currentValue, String[] options, StringListener listener) {
        InputMethod input = method;
        if (input == null) {
            input = UserManager.getInstance().getUser(player).getUserInputMethod();
        }
        new StringRequester().request(player, input, currentValue, "Type cancel to cancel", options, true, listener);
    }

    /**
     * Request string.
     *
     * @param player
     *            the player
     * @param listener
     *            the listener
     */
    public void requestString(Player player, StringListener listener) {
        InputMethod input = method;
        if (input == null) {
            input = UserManager.getInstance().getUser(player).getUserInputMethod();
        }
        new StringRequester().request(player, input, "", "Type cancel to cancel", null, true, listener);
    }

}
