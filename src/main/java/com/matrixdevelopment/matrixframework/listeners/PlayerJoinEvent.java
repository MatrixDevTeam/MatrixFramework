package com.matrixdevelopment.matrixframework.listeners;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.usermanagement.UUID;
import com.matrixdevelopment.matrixframework.usermanagement.User;
import com.matrixdevelopment.matrixframework.usermanagement.UserManager;
import com.matrixdevelopment.matrixframework.usermanagement.UserStorage;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

public class PlayerJoinEvent implements Listener {

    private static Plugin plugin;

    public PlayerJoinEvent(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerLogin(PlayerLoginEvent e) {
        MatrixFramework.getInstance().debug("Login: " + e.getPlayer().getName() + " (" + e.getPlayer().getUniqueId() + ")");

        Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
            Player player = e.getPlayer();

            if (player != null && UserManager.getInstance().getAllUUIDS().contains(player.getUniqueId().toString())) {
                boolean userExist = UserManager.getInstance().userExist(new UUID(e.getPlayer().getUniqueId().toString()));

                if (MatrixFramework.getInstance().getStorageType().equals(UserStorage.MYSQL) && MatrixFramework.getInstance().getMySQL() != null) {
                    if (userExist) {
                        MatrixFramework.getInstance().getMySQL().loadPlayerIfNeeded(player.getUniqueId().toString());
                    }
                }

                if (userExist) {
                    User user = UserManager.getInstance().getUser(player);

                    user.checkOfflineRewards();
                    user.setLastOnline(System.currentTimeMillis());
                    user.updateName();
                }

                MatrixFramework.getInstance().getUuidNameCache().put(player.getUniqueId().toString(), player.getName());
            }
        }, 10L);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent e) {
        MatrixFramework.getInstance().debug("Logout: " + e.getPlayer().getName() + " (" + e.getPlayer().getUniqueId() + ")");
    }

}
