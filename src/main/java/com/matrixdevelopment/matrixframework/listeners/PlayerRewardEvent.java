package com.matrixdevelopment.matrixframework.listeners;

import com.matrixdevelopment.matrixframework.rewards.Reward;
import com.matrixdevelopment.matrixframework.rewards.RewardOptions;
import com.matrixdevelopment.matrixframework.usermanagement.User;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerRewardEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public static HandlerList getHandlerList() {
        return handlers;
    }

    private User player;
    private Reward reward;
    private RewardOptions rewardOptions;
    private boolean canceled;

    public PlayerRewardEvent(Reward reward, User player, RewardOptions rewardOptions) {
        super();
        setPlayer(player);
        setReward(reward);
        setRewardOptions(rewardOptions);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    /**
     * Gets the player.
     *
     * @return the player
     */
    public User getPlayer() {
        return player;
    }

    /**
     * Gets the reward.
     *
     * @return the reward
     */
    public Reward getReward() {
        return reward;
    }

    /**
     * @return the rewardOptions
     */
    public RewardOptions getRewardOptions() {
        return rewardOptions;
    }

    /**
     * Checks if is canceled.
     *
     * @return true, if is canceled
     */
    public boolean isCanceled() {
        return canceled;
    }

    /**
     * Sets the canceled.
     *
     * @param bln the new canceled
     */
    public void setCanceled(boolean bln) {
        canceled = bln;
    }

    /**
     * Sets the player.
     *
     * @param player
     *            the new player
     */
    public void setPlayer(User player) {
        this.player = player;
    }

    /**
     * Sets the reward.
     *
     * @param reward
     *            the new reward
     */
    public void setReward(Reward reward) {
        this.reward = reward;
    }

    /**
     * @param rewardOptions
     *            the rewardOptions to set
     */
    public void setRewardOptions(RewardOptions rewardOptions) {
        this.rewardOptions = rewardOptions;
    }

}
