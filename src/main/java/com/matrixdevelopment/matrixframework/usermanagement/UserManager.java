package com.matrixdevelopment.matrixframework.usermanagement;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.userstore.sql.Column;
import com.matrixdevelopment.matrixframework.util.misc.ArrayUtils;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class UserManager {

    /** The instance. */
    static UserManager instance = new UserManager();

    /**
     * Gets the single instance of UserManager.
     *
     * @return single instance of UserManager
     */
    public static UserManager getInstance() {
        return instance;
    }

    /** The plugin. */
    MatrixFramework plugin = MatrixFramework.getInstance();

    /**
     * Instantiates a new user manager.
     */
    public UserManager() {
    }

    public ArrayList<String> getAllPlayerNames() {
        ArrayList<String> names = new ArrayList();
        if (MatrixFramework.getInstance().getStorageType().equals(UserStorage.FLAT)) {
            for (String uuid : getAllUUIDS()) {
                User user = UserManager.getInstance().getUser(new UUID(uuid));
                String name = user.getPlayerName();
                if (name != null && !name.isEmpty() && !name.equalsIgnoreCase("Error getting name")) {
                    names.add(name);
                }
            }
        } else if (MatrixFramework.getInstance().getStorageType().equals(UserStorage.SQLITE)) {
            ArrayList<String> data = MatrixFramework.getInstance().getSQLiteUserTable().getNames();
            for (String name : data) {
                if (name != null && !name.isEmpty() && !name.equalsIgnoreCase("Error getting name")) {
                    names.add(name);
                }
            }
        } else if (MatrixFramework.getInstance().getStorageType().equals(UserStorage.MYSQL)) {
            ArrayList<String> data = ArrayUtils.getInstance().convert(MatrixFramework.getInstance().getMySQL().getNames());
            for (String name : data) {
                if (name != null && !name.isEmpty() && !name.equalsIgnoreCase("Error getting name")) {
                    names.add(name);
                }
            }
        }
        return names;
    }

    public ArrayList<String> getAllUUIDS() {
        if (MatrixFramework.getInstance().getStorageType().equals(UserStorage.FLAT)) {
            File folder = new File(plugin.getPlugin().getDataFolder() + File.separator + "Data");
            String[] fileNames = folder.list();
            ArrayList<String> uuids = new ArrayList<String>();
            if (fileNames != null) {
                for (String playerFile : fileNames) {
                    if (!playerFile.equals("null") && !playerFile.equals("")) {
                        String uuid = playerFile.replace(".yml", "");
                        uuids.add(uuid);
                    }
                }
            }
            return uuids;
        } else if (MatrixFramework.getInstance().getStorageType().equals(UserStorage.SQLITE)) {
            List<Column> cols = MatrixFramework.getInstance().getSQLiteUserTable().getRows();
            ArrayList<String> uuids = new ArrayList<String>();
            for (Column col : cols) {
                uuids.add((String) col.getValue());
            }
            return uuids;
        } else if (MatrixFramework.getInstance().getStorageType().equals(UserStorage.MYSQL)) {
            return ArrayUtils.getInstance().convert(MatrixFramework.getInstance().getMySQL().getUUIDS());
        }
        return new ArrayList();
    }

    public String getProperName(String name) {

        for (String s : plugin.getUuidNameCache().values()) {
            if (s.equalsIgnoreCase(name)) {
                return s;
            }
        }
        return name;
    }

    public User getUser(java.util.UUID uuid) {
        return getUser(new UUID(uuid.toString()));
    }

    /**
     * Gets the user.
     *
     * @param player
     *            the player
     * @return the user
     */
    public User getUser(OfflinePlayer player) {
        return getUser(player.getName());
    }

    /**
     * Gets the user.
     *
     * @param player
     *            the player
     * @return the user
     */
    public User getUser(Player player) {
        return getUser(player.getName());
    }

    /**
     * Gets the user.
     *
     * @param playerName
     *            the player name
     * @return the user
     */
    public User getUser(String playerName) {
        return new User(plugin.getPlugin(), getProperName(playerName));
    }

    /**
     * Gets the user.
     *
     * @param uuid
     *            the uuid
     * @return the user
     */
    public User getUser(UUID uuid) {
        return new User(plugin.getPlugin(), uuid);
    }

    public void purgeOldPlayers() {
        if (plugin.isPurgeOldData()) {
            plugin.addUserStartup(new UserStartup() {

                @Override
                public void onFinish() {
                    plugin.debug("Finished purgining");
                }

                @Override
                public void onStart() {

                }

                @Override
                public void onStartUp(User user) {
                    int daysOld = plugin.getPurgeMinimumDays();
                    int days = user.getNumberOfDaysSinceLogin();
                    if (days == -1) {
                        // fix ones with no last online
                        user.setLastOnline(System.currentTimeMillis());
                    }
                    if (days > daysOld) {
                        plugin.debug("Removing " + user.getUUID() + " because of purge");
                        user.remove();
                    }
                }
            });
        }
        if (MatrixFramework.getInstance().getStorageType().equals(UserStorage.MYSQL)
                && MatrixFramework.getInstance().getMySQL() != null) {
            MatrixFramework.getInstance().getMySQL().clearCacheBasic();
        }
    }

    public boolean userExist(String name) {
        boolean exist = UserManager.getInstance().getAllPlayerNames().contains(name);
        if (exist) {
            return exist;
        }

        for (String s : plugin.getUuidNameCache().values()) {
            if (s.equalsIgnoreCase(name)) {
                return true;
            }
        }

        for (String uuid : getAllUUIDS()) {
            User user1 = getUser(new UUID(uuid));
            if (user1.getPlayerName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean userExist(UUID uuid) {
        if (uuid != null && uuid.getUUID() != null) {
            if (getAllUUIDS().contains(uuid.getUUID())) {
                return true;
            }
        }

        return false;
    }

}
