package com.matrixdevelopment.matrixframework.usermanagement;

public class UUID {

    private String uuid;

    /**
     * Instantiates a new uuid.
     *
     * @param uuid the UUID
     */
    public UUID(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Gets the uuid.
     *
     * @return the uuid
     */
    public String getUUID() {
        return uuid;
    }

    /**
     * Sets the uuid.
     *
     * @param uuid the new UUID
     */
    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

}
