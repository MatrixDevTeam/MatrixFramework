package com.matrixdevelopment.matrixframework.data;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.yaml.YMLFile;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ServerData extends YMLFile {

    static ServerData INSTANCE = new ServerData();

    /**
     * Gets the instance of the class
     *
     * @return instance of the class
     */
    public static ServerData getInstance() {
        return INSTANCE;
    }

    MatrixFramework plugin = MatrixFramework.getInstance();

    /**
     * Instantiates a new instance.
     */
    public ServerData() {
        super(new File(MatrixFramework.getInstance().getPlugin().getDataFolder(), "serverdata.yml"));
    }

    public List getIntColumns() {
        return getData().getList("IntColumns", new ArrayList());
    }

    /**
     * Gets the plugin's version
     *
     * @param plugin the Plugin
     * @return the Plugin's version
     */
    public String getPluginVersion(Plugin plugin) {
        return getData().getString("PluginVersions." + plugin.getName(), "");
    }

    /**
     * Gets the previous day.
     *
     * @return the previous day
     */
    public int getPreviousDay() {
        return getData().getInt("PreviousDay", -1);
    }

    /**
     * Gets the previous month.
     *
     * @return the previous month
     */
    public String getPreviousMonth() {
        return getData().getString("Month", "");
    }

    /**
     * Gets the previous week day.
     *
     * @return the previous week day
     */
    public int getPreviousWeekDay() {
        return getData().getInt("PreviousWeek", -1);
    }

    /**
     * (non-Javadoc)
     *
     * @see com.matrixdevelopment.matrixframework.yaml.YMLFile#onFileCreation()
     */
    @Override
    public void onFileCreation() {}

    public void setData(String path, Object value) {
        getData().set(path, value);
        saveData();
    }

    public void setIntColumns(List<String> intColumns) {
        getData().set("IntColumns", intColumns);
        saveData();
    }

    /**
     * Sets the plugin version.
     *
     * @param plugin
     *            the new plugin version
     */
    public void setPluginVersion(Plugin plugin) {
        getData().set("PluginVersions." + plugin.getName(), plugin.getDescription().getVersion());
        saveData();
    }

    /**
     * Sets the previous day.
     *
     * @param day
     *            the new previous day
     */
    public void setPreviousDay(int day) {
        getData().set("PreviousDay", day);
        saveData();
    }

    /**
     * Sets the previous month.
     *
     * @param month
     *            the new previous month
     */
    public void setPreviousMonth(String month) {
        getData().set("Month", month);
        saveData();
    }

    /**
     * Sets the previous week day.
     *
     * @param week
     *            the new previous week day
     */
    public void setPreviousWeekDay(int week) {
        getData().set("PreviousWeek", week);
        saveData();
    }

}
