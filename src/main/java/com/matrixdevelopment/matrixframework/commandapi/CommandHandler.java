package com.matrixdevelopment.matrixframework.commandapi;

import com.matrixdevelopment.matrixframework.MatrixFramework;
import com.matrixdevelopment.matrixframework.usermanagement.UserManager;
import com.matrixdevelopment.matrixframework.util.misc.ArrayUtils;
import com.matrixdevelopment.matrixframework.util.misc.PlayerUtils;
import com.matrixdevelopment.matrixframework.util.misc.StringUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public abstract class CommandHandler {

    MatrixFramework plugin = MatrixFramework.getInstance();

    private String[] args;
    private String perm, helpMessage;
    private boolean allowConsole = true;
    private boolean matrixFrameworkCMD, ignoreNumberCheck = false;

    public CommandHandler() {}

    /**
     * Instantiates a new command handler.
     *
     * @param args the Arguments
     * @param perm the Permission
     */
    public CommandHandler(String[] args, String perm) {
        this.args = args;
        this.perm = perm;
        helpMessage = "Unknown help message";
    }

    /**
     * Instantiates a new command handler.
     *
     * @param args the Arguments
     * @param perm the Permission
     * @param helpMessage the Help Message
     */
    public CommandHandler(String[] args, String perm, String helpMessage) {
        this.args = args;
        this.perm = perm;
        this.helpMessage = helpMessage;
    }

    /**
     * Instantiates a new command handler.
     *
     * @param args the Arguments
     * @param perm the Permission
     * @param helpMessage the Help Message
     * @param allowConsole to Allow console
     */
    public CommandHandler(String[] args, String perm, String helpMessage, boolean allowConsole) {
        this.args = args;
        this.perm = perm;
        this.helpMessage = helpMessage;
        this.allowConsole = allowConsole;
    }

    /**
     * Adds the tab complete option
     *
     * @param toReplace the String to replace
     * @param options the Options
     */
    @Deprecated
    public void addTabCompleteOption(String toReplace, ArrayList<String> options) {
        TabCompleteHandler.getInstance().addTabCompleteOption(toReplace, options);
    }

    /**
     * Adds the tab complete option
     *
     * @param toReplace the String to replace
     * @param options the Options
     */
    @Deprecated
    public void addTabCompleteOption(String toReplace, String... options) {
        addTabCompleteOption(toReplace, ArrayUtils.getInstance().convert(options));
    }

    /**
     * Args matching
     *
     * @param arg the Argument
     * @param i the Int
     * @return true, if Successful
     */
    public boolean argsMatch(String arg, int i) {
        if (i < args.length) {
            String[] cmdArgs = args[i].split("&");

            for (String cmdArg : cmdArgs) {
                if (arg.equalsIgnoreCase(cmdArg)) {
                    return true;
                }

                for (String str : TabCompleteHandler.getInstance().getTabCompleteReplaces()) {
                    if (str.equalsIgnoreCase(cmdArg)) {
                        return true;
                    }
                }
            }

            for (String str : TabCompleteHandler.getInstance().getTabCompleteReplaces()) {
                if (str.equalsIgnoreCase(args[i])) {
                    return true;
                }
            }

            return false;
        } else if (args[args.length - 1].equalsIgnoreCase("(list)")) {
            return true;
        }

        return false;
    }

    /**
     * Execute
     *
     * @param sender the Sender
     * @param args the Arguments
     */
    public abstract void execute(CommandSender sender, String[] args);

    /**
     * Gets the arguments
     *
     * @return the Arguments
     */
    public String[] getArgs() {
        return args;
    }

    /**
     * Gets the help line
     *
     * @param command the Command
     * @return the help line
     */
    public TextComponent getHelpLine(String command) {
        String line = plugin.getHelpLine();
        String cmdText = getHelpLineCommand(command);
        line = line.replace("%command%", cmdText);

        if (getHelpMessage() != "") {
            line = line.replace("%help_message%", getHelpMessage());
        }

        TextComponent txt = StringUtils.getInstance().stringToComp(line);
        txt.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, cmdText));
        txt.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(getHelpMessage()).color(ChatColor.GREEN).create()));

        return txt;
    }

    /**
     * Gets the help line Command
     *
     * @param command the Command
     * @return the help command
     */
    public String getHelpLineCommand(String command) {
        String commandTxt = command;

        for (String arg1 : args) {
            int count = 1;

            for (String arg : arg1.split("&")) {
                if (count == 1) {
                    commandTxt += " " + arg;
                } else {
                    commandTxt += "/" + arg;
                }

                count++;
            }
        }
        return commandTxt;
    }

    /**
     * Gets the help message
     *
     * @return the Help message
     */
    public String getHelpMessage() {
        return helpMessage;
    }

    /**
     * Gets the permission
     *
     * @return the Permission
     */
    public String getPerm() {
        return perm;
    }

    public ArrayList<String> getTabCompleteOptions(CommandSender sender, String[] args, int argNum, ConcurrentHashMap<String, ArrayList<String>> tabCompleteOptions) {
        Set<String> cmds = new HashSet();

        if (hasPerm(sender)) {
            CommandHandler commandHandler = this;

            String[] cmdArgs = commandHandler.getArgs();

            if (cmdArgs.length > argNum) {
                boolean argsMatch = true;

                for (int i = 0; i < argNum; i++) {
                    if (args.length >= i) {
                        if (!commandHandler.argsMatch(args[i], i)) {
                             argsMatch = false;
                        }
                    }
                }

                if (argsMatch) {
                    String[] cmdArgsList = cmdArgs[argNum].split("&");

                    for (String arg : cmdArgsList) {
                        boolean add = true;

                        for (Map.Entry<String, ArrayList<String>> entry : tabCompleteOptions.entrySet()) {
                            if (arg.equalsIgnoreCase(entry.getKey())) {
                                add = false;
                                cmds.addAll(entry.getValue());
                            }
                        }

                        if (!cmds.contains(arg) && add) {
                            cmds.add(arg);
                        }
                    }
                }
            }
        }

        ArrayList<String> options = ArrayUtils.getInstance().convert(cmds);
        Collections.sort(options, String.CASE_INSENSITIVE_ORDER);
        return options;
    }

    public boolean hasArg(String arg) {
        for (String str : getArgs()) {
            if (str.equalsIgnoreCase(arg)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasPerm(CommandSender sender) {
        return PlayerUtils.getInstance().hasEitherPermission(sender, getPerm());
    }

    public CommandHandler ignoreNumberCheck() {
        ignoreNumberCheck = true;
        return this;
    }

    public boolean isMatrixFrameworkCMD() {
        return matrixFrameworkCMD;
    }

    public boolean isAllowConsole() {
        return allowConsole;
    }

    public boolean isPlayer(CommandSender sender) {
        return sender instanceof Player;
    }

    /**
     * Run a command
     *
     * @param sender the command sender
     * @param args the Arguments
     * @return true if successful
     */
    public boolean runCommand(CommandSender sender, String[] args) {
        if (args.length >= this.args.length) {
            if (this.args.length != args.length && !hasArg("(list)")) {
                return false;
            }

            for (int i = 0; i < args.length && i < this.args.length; i++) {
                if (!argsMatch(args[i], i)) {
                    return false;
                }

                if (this.args[i].equalsIgnoreCase("(number)")) {
                    if (!ignoreNumberCheck && !StringUtils.getInstance().isInt(args[i])) {
                        sender.sendMessage(StringUtils.getInstance().colorize(plugin.getFormatNotNumber().replace("%arg%", args[i])));
                        return true;
                    }
                } else if (this.args[i].equalsIgnoreCase("player")) {
                    if (args[i].equalsIgnoreCase("@p")) {
                        args[i] = sender.getName();
                    } else if (args[i].equalsIgnoreCase("@r")) {
                        args[i] = PlayerUtils.getInstance().getRandomOnlinePlayer().getName();
                    }
                }
            }

            if (!(sender instanceof Player) && !allowConsole) {
                sender.sendMessage("This command is only able to be ran in game.");
                return true;
            }

            if (!hasPerm(sender)) {
                sender.sendMessage(StringUtils.getInstance().colorize(plugin.getFormatNoPerms()));
                plugin.getPlugin().getLogger().info(sender.getName() + " was denied access to command " + args + ", permission " + perm + " is required.");
                return true;
            }

            Bukkit.getServer().getScheduler().runTaskAsynchronously(plugin.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    execute(sender, args);
                }
            });

            return true;
        }
        return false;
    }

    public void sendMessage(CommandSender sender, ArrayList<String> msg) {
        sender.sendMessage(ArrayUtils.getInstance().convert(ArrayUtils.getInstance().colorize(msg)));
    }

    public void sendMessage(CommandSender sender, String msg) {
        sender.sendMessage(StringUtils.getInstance().colorize(msg));
    }

    public void sendMessageJson(CommandSender sender, ArrayList<TextComponent> comp) {
        if (isPlayer(sender)) {
            Player player = (Player) sender;
            UserManager.getInstance().getUser(player).sendJson(comp);
        } else {
            sender.sendMessage(ArrayUtils.getInstance().convert(ArrayUtils.getInstance().comptoString(comp)));
        }
    }

    public void sendMessageJson(CommandSender sender, TextComponent comp) {
        if (isPlayer(sender)) {
            Player player = (Player) sender;
            UserManager.getInstance().getUser(player).sendJson(comp);
        } else {
            sender.sendMessage(StringUtils.getInstance().compToString(comp));
        }
    }

    public void setMatrixFrameworkCMD(boolean matrixFrameworkCMD) {
        this.matrixFrameworkCMD = matrixFrameworkCMD;
    }

    public CommandHandler setAllowConsole(boolean allowConsole) {
        this.allowConsole = allowConsole;
        return this;
    }

    public CommandHandler setArgs(String[] args) {
        this.args = args;
        return this;
    }

    public CommandHandler setHelpMessage(String helpMessage) {
        this.helpMessage = helpMessage;
        return this;
    }

    public CommandHandler setPerm(String perm) {
        this.perm = perm;
        return this;
    }
}
