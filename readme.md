# Matrix Framework

A development framework for the Spigot/Bukkit API

**Javadocs are located at: https://gmatrixgames.github.io/javadocs**

[![Discord](https://img.shields.io/discord/491638768831299584.svg?longCache=true&style=flat-square)](https://discord.gg/g4ghrTv)
[![Java](https://img.shields.io/badge/Java_Version-1.8-red.svg?style=popout-square)](https://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html)
[![GitHub last commit](https://img.shields.io/github/last-commit/GMatrixGames/matrixframework.svg?style=flat-square)](https://gitlab.com/GMatrixGames/MatrixFramework)
[![GitHub issues](https://img.shields.io/github/issues/GMatrixGames/MatrixFramework/.svg?style=flat-square)](https://gitlab.com/GMatrixGames/MatrixFramework)
[![GitHub pull requests](https://img.shields.io/github/issues-pr/GMatrixGames/MatrixFramework/.svg?style=flat-square)](https://gitlab.com/GMatrixGames/MatrixFramework)

This is a framework that includes, but not limited to, these features:

#
 
* Zip file backups
* A Command handler and Tab completion
* Easy collection of server data
* NMS/Reflection utilities
* Recognition of the difference between CraftBukkit and Spigot
* File threads
* User management
* Easy MySQL/SQLite/Flatfile storage types
* Reflection for books and anvil inventories
* PlaceholderAPI usage in messages
* An animation api
* Item Builder
* Easy BossBar, ActionBar, Titles creation
* Custom fireworks
* Item Slot messages
* String, Array, Plugin, Player, Miscellaneous utilities
* Name fetcher
* Plugin Messaging

(More to come)
